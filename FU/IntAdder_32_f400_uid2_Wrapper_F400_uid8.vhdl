--------------------------------------------------------------------------------
--                           IntAdder_32_f400_uid2
--                      (IntAdderClassical_32_F400_uid4)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_32_f400_uid2 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(31 downto 0);
          Y : in  std_logic_vector(31 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(31 downto 0)   );
end entity;

architecture arch of IntAdder_32_f400_uid2 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X + Y + Cin;
end architecture;

--------------------------------------------------------------------------------
--                  IntAdder_32_f400_uid2_Wrapper_F400_uid8
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007)
--------------------------------------------------------------------------------
-- Pipeline depth: 2 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntAdder_32_f400_uid2_Wrapper_F400_uid8 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(31 downto 0);
          Y : in  std_logic_vector(31 downto 0);
          Cin : in  std_logic;
          R : out  std_logic_vector(31 downto 0)   );
end entity;

architecture arch of IntAdder_32_f400_uid2_Wrapper_F400_uid8 is
   component IntAdder_32_f400_uid2 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(31 downto 0);
             Y : in  std_logic_vector(31 downto 0);
             Cin : in  std_logic;
             R : out  std_logic_vector(31 downto 0)   );
   end component;

signal i_X, i_X_d1 :  std_logic_vector(31 downto 0);
signal i_Y, i_Y_d1 :  std_logic_vector(31 downto 0);
signal i_Cin, i_Cin_d1 :  std_logic;
signal o_R, o_R_d1 :  std_logic_vector(31 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            i_X_d1 <=  i_X;
            i_Y_d1 <=  i_Y;
            i_Cin_d1 <=  i_Cin;
            o_R_d1 <=  o_R;
         end if;
      end process;
   i_X <= X;
   i_Y <= Y;
   i_Cin <= Cin;
   ----------------Synchro barrier, entering cycle 1----------------
   test: IntAdder_32_f400_uid2  -- pipelineDepth=0 maxInDelay=0
      port map ( clk  => clk,
                 rst  => rst,
                 Cin => i_Cin_d1,
                 R => o_R,
                 X => i_X_d1,
                 Y => i_Y_d1);
   ----------------Synchro barrier, entering cycle 2----------------
   R <= o_R_d1;
end architecture;

