files = ["mul8_184", "mul8_195",
"mul8_219",
"mul8_222",
"mul8_259",
"mul8_280",
"mul8_366",
"mul8_377",
"mul8_480",
"mul8_499"]

def gn(x):
    return x.strip().split(" ")
for fn in files:
    with open(fn + ".c") as f:
        ln = f.readlines()
        print " ".join([fn, gn(ln[14])[-1], gn(ln[11])[-1], gn(ln[16])[-2] + "%"])

