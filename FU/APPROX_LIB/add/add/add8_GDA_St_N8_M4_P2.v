// Library = EvoApprox8b
// Circuit = add8_GDA_S_N8_M4_P2
// Area   (180) = 1472
// Delay  (180) = 0.950
// Power  (180) = 458.30
// Area   (45) = 109
// Delay  (45) = 0.360
// Power  (45) = 43.46
// Nodes = 23
// HD = 22656
// MAE = 7.50000
// MSE = 408.00000
// MRE = 3.65 %
// WCE = 64
// WCRE = 100 %
// EP = 18.8 %

module add8_GDA_St_N8_M4_P2(A, B, O);
  input [7:0] A;
  input [7:0] B;
  output [8:0] O;
  wire [2031:0] N;

  assign N[0] = A[0];
  assign N[1] = A[0];
  assign N[2] = A[1];
  assign N[3] = A[1];
  assign N[4] = A[2];
  assign N[5] = A[2];
  assign N[6] = A[3];
  assign N[7] = A[3];
  assign N[8] = A[4];
  assign N[9] = A[4];
  assign N[10] = A[5];
  assign N[11] = A[5];
  assign N[12] = A[6];
  assign N[13] = A[6];
  assign N[14] = A[7];
  assign N[15] = A[7];
  assign N[16] = B[0];
  assign N[17] = B[0];
  assign N[18] = B[1];
  assign N[19] = B[1];
  assign N[20] = B[2];
  assign N[21] = B[2];
  assign N[22] = B[3];
  assign N[23] = B[3];
  assign N[24] = B[4];
  assign N[25] = B[4];
  assign N[26] = B[5];
  assign N[27] = B[5];
  assign N[28] = B[6];
  assign N[29] = B[6];
  assign N[30] = B[7];
  assign N[31] = B[7];

  AND2X1 n32(.A(N[0]), .B(N[16]), .Y(N[32]));
  AND2X1 n34(.A(N[2]), .B(N[18]), .Y(N[34]));
  XOR2X1 n38(.A(N[2]), .B(N[18]), .Y(N[38]));
  AND2X1 n40(.A(N[38]), .B(N[32]), .Y(N[40]));
  OR2X1 n42(.A(N[34]), .B(N[40]), .Y(N[42]));
  AND2X1 n44(.A(N[4]), .B(N[20]), .Y(N[44]));
  AND2X1 n46(.A(N[6]), .B(N[22]), .Y(N[46]));
  XOR2X1 n50(.A(N[6]), .B(N[22]), .Y(N[50]));
  AND2X1 n52(.A(N[50]), .B(N[44]), .Y(N[52]));
  OR2X1 n54(.A(N[46]), .B(N[52]), .Y(N[54]));
  AND2X1 n56(.A(N[8]), .B(N[24]), .Y(N[56]));
  AND2X1 n58(.A(N[10]), .B(N[26]), .Y(N[58]));
  XOR2X1 n62(.A(N[10]), .B(N[26]), .Y(N[62]));
  AND2X1 n64(.A(N[62]), .B(N[56]), .Y(N[64]));
  OR2X1 n66(.A(N[58]), .B(N[64]), .Y(N[66]));
  HAX1 n68(.A(N[0]), .B(N[16]), .YS(N[68]), .YC(N[69]));
  FAX1 n70(.A(N[2]), .B(N[18]), .C(N[69]), .YS(N[70]), .YC(N[71]));
  FAX1 n74(.A(N[4]), .B(N[20]), .C(N[42]), .YS(N[74]), .YC(N[75]));
  FAX1 n76(.A(N[6]), .B(N[22]), .C(N[75]), .YS(N[76]), .YC(N[77]));
  FAX1 n80(.A(N[8]), .B(N[24]), .C(N[54]), .YS(N[80]), .YC(N[81]));
  FAX1 n82(.A(N[10]), .B(N[26]), .C(N[81]), .YS(N[82]), .YC(N[83]));
  FAX1 n86(.A(N[12]), .B(N[28]), .C(N[66]), .YS(N[86]), .YC(N[87]));
  FAX1 n88(.A(N[14]), .B(N[30]), .C(N[87]), .YS(N[88]), .YC(N[89]));

  assign O[0] = N[68];
  assign O[1] = N[70];
  assign O[2] = N[74];
  assign O[3] = N[76];
  assign O[4] = N[80];
  assign O[5] = N[82];
  assign O[6] = N[86];
  assign O[7] = N[88];
  assign O[8] = N[89];

endmodule
