// Library = EvoApprox8b
// Circuit = add8_GDA_S_N8_M4_P4
// Area   (180) = 1776
// Delay  (180) = 1.230
// Power  (180) = 582.50
// Area   (45) = 133
// Delay  (45) = 0.480
// Power  (45) = 53.92
// Nodes = 31
// HD = 2688
// MAE = 1.50000
// MSE = 96.00000
// MRE = 0.74 %
// WCE = 64
// WCRE = 100 %
// EP = 2.3 %

module add8_GDA_St_N8_M4_P4(A, B, O);
  input [7:0] A;
  input [7:0] B;
  output [8:0] O;
  wire [2031:0] N;

  assign N[0] = A[0];
  assign N[1] = A[0];
  assign N[2] = A[1];
  assign N[3] = A[1];
  assign N[4] = A[2];
  assign N[5] = A[2];
  assign N[6] = A[3];
  assign N[7] = A[3];
  assign N[8] = A[4];
  assign N[9] = A[4];
  assign N[10] = A[5];
  assign N[11] = A[5];
  assign N[12] = A[6];
  assign N[13] = A[6];
  assign N[14] = A[7];
  assign N[15] = A[7];
  assign N[16] = B[0];
  assign N[17] = B[0];
  assign N[18] = B[1];
  assign N[19] = B[1];
  assign N[20] = B[2];
  assign N[21] = B[2];
  assign N[22] = B[3];
  assign N[23] = B[3];
  assign N[24] = B[4];
  assign N[25] = B[4];
  assign N[26] = B[5];
  assign N[27] = B[5];
  assign N[28] = B[6];
  assign N[29] = B[6];
  assign N[30] = B[7];
  assign N[31] = B[7];

  AND2X1 n32(.A(N[0]), .B(N[16]), .Y(N[32]));
  AND2X1 n34(.A(N[2]), .B(N[18]), .Y(N[34]));
  XOR2X1 n38(.A(N[2]), .B(N[18]), .Y(N[38]));
  AND2X1 n40(.A(N[38]), .B(N[32]), .Y(N[40]));
  OR2X1 n42(.A(N[34]), .B(N[40]), .Y(N[42]));
  AND2X1 n44(.A(N[4]), .B(N[20]), .Y(N[44]));
  AND2X1 n46(.A(N[6]), .B(N[22]), .Y(N[46]));
  XOR2X1 n48(.A(N[4]), .B(N[20]), .Y(N[48]));
  XOR2X1 n50(.A(N[6]), .B(N[22]), .Y(N[50]));
  AND2X1 n52(.A(N[50]), .B(N[44]), .Y(N[52]));
  OR2X1 n54(.A(N[46]), .B(N[52]), .Y(N[54]));
  AND2X1 n56(.A(N[50]), .B(N[48]), .Y(N[56]));
  AND2X1 n58(.A(N[56]), .B(N[42]), .Y(N[58]));
  OR2X1 n60(.A(N[54]), .B(N[58]), .Y(N[60]));
  AND2X1 n62(.A(N[8]), .B(N[24]), .Y(N[62]));
  AND2X1 n64(.A(N[10]), .B(N[26]), .Y(N[64]));
  XOR2X1 n66(.A(N[8]), .B(N[24]), .Y(N[66]));
  XOR2X1 n68(.A(N[10]), .B(N[26]), .Y(N[68]));
  AND2X1 n70(.A(N[68]), .B(N[62]), .Y(N[70]));
  OR2X1 n72(.A(N[64]), .B(N[70]), .Y(N[72]));
  AND2X1 n74(.A(N[68]), .B(N[66]), .Y(N[74]));
  AND2X1 n76(.A(N[74]), .B(N[54]), .Y(N[76]));
  OR2X1 n78(.A(N[72]), .B(N[76]), .Y(N[78]));
  HAX1 n80(.A(N[0]), .B(N[16]), .YS(N[80]), .YC(N[81]));
  FAX1 n82(.A(N[2]), .B(N[18]), .C(N[81]), .YS(N[82]), .YC(N[83]));
  FAX1 n86(.A(N[4]), .B(N[20]), .C(N[42]), .YS(N[86]), .YC(N[87]));
  FAX1 n88(.A(N[6]), .B(N[22]), .C(N[87]), .YS(N[88]), .YC(N[89]));
  FAX1 n92(.A(N[8]), .B(N[24]), .C(N[60]), .YS(N[92]), .YC(N[93]));
  FAX1 n94(.A(N[10]), .B(N[26]), .C(N[93]), .YS(N[94]), .YC(N[95]));
  FAX1 n98(.A(N[12]), .B(N[28]), .C(N[78]), .YS(N[98]), .YC(N[99]));
  FAX1 n100(.A(N[14]), .B(N[30]), .C(N[99]), .YS(N[100]), .YC(N[101]));

  assign O[0] = N[80];
  assign O[1] = N[82];
  assign O[2] = N[86];
  assign O[3] = N[88];
  assign O[4] = N[92];
  assign O[5] = N[94];
  assign O[6] = N[98];
  assign O[7] = N[100];
  assign O[8] = N[101];

endmodule
