/// Approximate function add8_GDA_St_N8_M4_P2
///  Library = EvoApprox8b
///  Circuit = add8_GDA_S_N8_M4_P2
///  Area   (180) = 1472
///  Delay  (180) = 0.950
///  Power  (180) = 458.30
///  Area   (45) = 109
///  Delay  (45) = 0.360
///  Power  (45) = 43.46
///  Nodes = 23
///  HD = 22656
///  MAE = 7.50000
///  MSE = 408.00000
///  MRE = 3.65 %
///  WCE = 64
///  WCRE = 100 %
///  EP = 18.8 %
uint16_t add8_GDA_St_N8_M4_P2(uint8_t a, uint8_t b)
{
  uint16_t c = 0;
  uint8_t n0 = (a >> 0) & 0x1;
  uint8_t n2 = (a >> 1) & 0x1;
  uint8_t n4 = (a >> 2) & 0x1;
  uint8_t n6 = (a >> 3) & 0x1;
  uint8_t n8 = (a >> 4) & 0x1;
  uint8_t n10 = (a >> 5) & 0x1;
  uint8_t n12 = (a >> 6) & 0x1;
  uint8_t n14 = (a >> 7) & 0x1;
  uint8_t n16 = (b >> 0) & 0x1;
  uint8_t n18 = (b >> 1) & 0x1;
  uint8_t n20 = (b >> 2) & 0x1;
  uint8_t n22 = (b >> 3) & 0x1;
  uint8_t n24 = (b >> 4) & 0x1;
  uint8_t n26 = (b >> 5) & 0x1;
  uint8_t n28 = (b >> 6) & 0x1;
  uint8_t n30 = (b >> 7) & 0x1;
  uint8_t n32;
  uint8_t n34;
  uint8_t n38;
  uint8_t n40;
  uint8_t n42;
  uint8_t n44;
  uint8_t n46;
  uint8_t n50;
  uint8_t n52;
  uint8_t n54;
  uint8_t n56;
  uint8_t n58;
  uint8_t n62;
  uint8_t n64;
  uint8_t n66;
  uint8_t n68;
  uint8_t n69;
  uint8_t n70;
  uint8_t n74;
  uint8_t n75;
  uint8_t n76;
  uint8_t n80;
  uint8_t n81;
  uint8_t n82;
  uint8_t n86;
  uint8_t n87;
  uint8_t n88;
  uint8_t n89;

  n32 = n0 & n16;
  n34 = n2 & n18;
  n38 = n2 ^ n18;
  n40 = n38 & n32;
  n42 = n34 | n40;
  n44 = n4 & n20;
  n46 = n6 & n22;
  n50 = n6 ^ n22;
  n52 = n50 & n44;
  n54 = n46 | n52;
  n56 = n8 & n24;
  n58 = n10 & n26;
  n62 = n10 ^ n26;
  n64 = n62 & n56;
  n66 = n58 | n64;
  n68 = n0 ^ n16;
  n69 = n0 & n16;
  n70 = (n2 ^ n18) ^ n69;
  n74 = (n4 ^ n20) ^ n42;
  n75 = (n4 & n20) | (n20 & n42) | (n4 & n42);
  n76 = (n6 ^ n22) ^ n75;
  n80 = (n8 ^ n24) ^ n54;
  n81 = (n8 & n24) | (n24 & n54) | (n8 & n54);
  n82 = (n10 ^ n26) ^ n81;
  n86 = (n12 ^ n28) ^ n66;
  n87 = (n12 & n28) | (n28 & n66) | (n12 & n66);
  n88 = (n14 ^ n30) ^ n87;
  n89 = (n14 & n30) | (n30 & n87) | (n14 & n87);

  c |= (n68 & 0x1) << 0;
  c |= (n70 & 0x1) << 1;
  c |= (n74 & 0x1) << 2;
  c |= (n76 & 0x1) << 3;
  c |= (n80 & 0x1) << 4;
  c |= (n82 & 0x1) << 5;
  c |= (n86 & 0x1) << 6;
  c |= (n88 & 0x1) << 7;
  c |= (n89 & 0x1) << 8;

  return c;
}

