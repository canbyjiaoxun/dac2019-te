function [ c ] = add8_GDA_St_N8_M8_P2( a, b )
% Approximate function add8_GDA_St_N8_M8_P2
%  Library = EvoApprox8b
%  Circuit = add8_GDA_S_N8_M8_P2
%  Area   (180) = 2032
%  Delay  (180) = 0.820
%  Power  (180) = 607.80
%  Area   (45) = 156
%  Delay  (45) = 0.320
%  Power  (45) = 55.93
%  Nodes = 40
%  HD = 36992
%  MAE = 15.50000
%  MSE = 1392.00000
%  MRE = 7.07 %
%  WCE = 144
%  WCRE = 100 %
%  EP = 30.1 %
  a = uint16(a);
  b = uint16(b);
  c = 0;
  n0 = bitand(bitshift(a, -0), 1, 'uint16');
  n2 = bitand(bitshift(a, -1), 1, 'uint16');
  n4 = bitand(bitshift(a, -2), 1, 'uint16');
  n6 = bitand(bitshift(a, -3), 1, 'uint16');
  n8 = bitand(bitshift(a, -4), 1, 'uint16');
  n10 = bitand(bitshift(a, -5), 1, 'uint16');
  n12 = bitand(bitshift(a, -6), 1, 'uint16');
  n14 = bitand(bitshift(a, -7), 1, 'uint16');
  n16 = bitand(bitshift(b, -0), 1, 'uint16');
  n18 = bitand(bitshift(b, -1), 1, 'uint16');
  n20 = bitand(bitshift(b, -2), 1, 'uint16');
  n22 = bitand(bitshift(b, -3), 1, 'uint16');
  n24 = bitand(bitshift(b, -4), 1, 'uint16');
  n26 = bitand(bitshift(b, -5), 1, 'uint16');
  n28 = bitand(bitshift(b, -6), 1, 'uint16');
  n30 = bitand(bitshift(b, -7), 1, 'uint16');
  n32 = bitand(n0, n16);
  n34 = bitand(n2, n18);
  n36 = bitand(n4, n20);
  n38 = bitand(n6, n22);
  n40 = bitand(n8, n24);
  n42 = bitand(n10, n26);
  n44 = bitand(n12, n28);
  n48 = bitxor(n2, n18);
  n50 = bitxor(n4, n20);
  n52 = bitxor(n6, n22);
  n54 = bitxor(n8, n24);
  n56 = bitxor(n10, n26);
  n58 = bitxor(n12, n28);
  n60 = n32;
  n62 = n34;
  n64 = bitand(n48, n60);
  n66 = bitor(n62, n64);
  n68 = n36;
  n70 = bitand(n50, n62);
  n72 = bitor(n68, n70);
  n74 = n38;
  n76 = bitand(n52, n68);
  n78 = bitor(n74, n76);
  n80 = n40;
  n82 = bitand(n54, n74);
  n84 = bitor(n80, n82);
  n86 = n42;
  n88 = bitand(n56, n80);
  n90 = bitor(n86, n88);
  n92 = n44;
  n94 = bitand(n58, n86);
  n96 = bitor(n92, n94);
  n98 = bitxor(n0, n16);
  n102 = bitxor(bitxor(n2, n18), n60);
  n106 = bitxor(bitxor(n4, n20), n66);
  n110 = bitxor(bitxor(n6, n22), n72);
  n114 = bitxor(bitxor(n8, n24), n78);
  n118 = bitxor(bitxor(n10, n26), n84);
  n122 = bitxor(bitxor(n12, n28), n90);
  n126 = bitxor(bitxor(n14, n30), n96);
  n127 = bitor(bitor(bitand(n14, n30), bitand(n30, n96)), bitand(n14, n96));
  c = bitor(c, bitshift(bitand(n98, 1), 0));
  c = bitor(c, bitshift(bitand(n102, 1), 1));
  c = bitor(c, bitshift(bitand(n106, 1), 2));
  c = bitor(c, bitshift(bitand(n110, 1), 3));
  c = bitor(c, bitshift(bitand(n114, 1), 4));
  c = bitor(c, bitshift(bitand(n118, 1), 5));
  c = bitor(c, bitshift(bitand(n122, 1), 6));
  c = bitor(c, bitshift(bitand(n126, 1), 7));
  c = bitor(c, bitshift(bitand(n127, 1), 8));
end