/// Approximate function add8_ACA_I_N8_Q5
///  Library = EvoApprox8b
///  Circuit = add8_ACA_I_N8_Q5
///  Area   (180) = 2240
///  Delay  (180) = 1.420
///  Power  (180) = 803.20
///  Area   (45) = 161
///  Delay  (45) = 0.500
///  Power  (45) = 77.40
///  Nodes = 20
///  HD = 5248
///  MAE = 3.50000
///  MSE = 336.00000
///  MRE = 1.65 %
///  WCE = 128
///  WCRE = 100 %
///  EP = 4.7 %
uint16_t add8_ACA_I_N8_Q5(uint8_t a, uint8_t b)
{
  uint16_t c = 0;
  uint8_t n0 = (a >> 0) & 0x1;
  uint8_t n2 = (a >> 1) & 0x1;
  uint8_t n4 = (a >> 2) & 0x1;
  uint8_t n6 = (a >> 3) & 0x1;
  uint8_t n8 = (a >> 4) & 0x1;
  uint8_t n10 = (a >> 5) & 0x1;
  uint8_t n12 = (a >> 6) & 0x1;
  uint8_t n14 = (a >> 7) & 0x1;
  uint8_t n16 = (b >> 0) & 0x1;
  uint8_t n18 = (b >> 1) & 0x1;
  uint8_t n20 = (b >> 2) & 0x1;
  uint8_t n22 = (b >> 3) & 0x1;
  uint8_t n24 = (b >> 4) & 0x1;
  uint8_t n26 = (b >> 5) & 0x1;
  uint8_t n28 = (b >> 6) & 0x1;
  uint8_t n30 = (b >> 7) & 0x1;
  uint8_t n32;
  uint8_t n33;
  uint8_t n34;
  uint8_t n35;
  uint8_t n36;
  uint8_t n37;
  uint8_t n38;
  uint8_t n39;
  uint8_t n40;
  uint8_t n45;
  uint8_t n47;
  uint8_t n49;
  uint8_t n51;
  uint8_t n52;
  uint8_t n57;
  uint8_t n59;
  uint8_t n61;
  uint8_t n63;
  uint8_t n64;
  uint8_t n69;
  uint8_t n71;
  uint8_t n73;
  uint8_t n75;
  uint8_t n76;
  uint8_t n77;

  n32 = n0 ^ n16;
  n33 = n0 & n16;
  n34 = (n2 ^ n18) ^ n33;
  n35 = (n2 & n18) | (n18 & n33) | (n2 & n33);
  n36 = (n4 ^ n20) ^ n35;
  n37 = (n4 & n20) | (n20 & n35) | (n4 & n35);
  n38 = (n6 ^ n22) ^ n37;
  n39 = (n6 & n22) | (n22 & n37) | (n6 & n37);
  n40 = (n8 ^ n24) ^ n39;
  n45 = n2 & n18;
  n47 = (n4 & n20) | (n20 & n45) | (n4 & n45);
  n49 = (n6 & n22) | (n22 & n47) | (n6 & n47);
  n51 = (n8 & n24) | (n24 & n49) | (n8 & n49);
  n52 = (n10 ^ n26) ^ n51;
  n57 = n4 & n20;
  n59 = (n6 & n22) | (n22 & n57) | (n6 & n57);
  n61 = (n8 & n24) | (n24 & n59) | (n8 & n59);
  n63 = (n10 & n26) | (n26 & n61) | (n10 & n61);
  n64 = (n12 ^ n28) ^ n63;
  n69 = n6 & n22;
  n71 = (n8 & n24) | (n24 & n69) | (n8 & n69);
  n73 = (n10 & n26) | (n26 & n71) | (n10 & n71);
  n75 = (n12 & n28) | (n28 & n73) | (n12 & n73);
  n76 = (n14 ^ n30) ^ n75;
  n77 = (n14 & n30) | (n30 & n75) | (n14 & n75);

  c |= (n32 & 0x1) << 0;
  c |= (n34 & 0x1) << 1;
  c |= (n36 & 0x1) << 2;
  c |= (n38 & 0x1) << 3;
  c |= (n40 & 0x1) << 4;
  c |= (n52 & 0x1) << 5;
  c |= (n64 & 0x1) << 6;
  c |= (n76 & 0x1) << 7;
  c |= (n77 & 0x1) << 8;

  return c;
}

