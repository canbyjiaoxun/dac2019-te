/// Approximate function add8_GeAr_N8_R2_P4
///  Library = EvoApprox8b
///  Circuit = add8_GA_N8_R2_P4
///  Area   (180) = 1360
///  Delay  (180) = 1.380
///  Power  (180) = 484.60
///  Area   (45) = 99
///  Delay  (45) = 0.500
///  Power  (45) = 47.24
///  Nodes = 12
///  HD = 2688
///  MAE = 1.50000
///  MSE = 96.00000
///  MRE = 0.74 %
///  WCE = 64
///  WCRE = 100 %
///  EP = 2.3 %
uint16_t add8_GeAr_N8_R2_P4(uint8_t a, uint8_t b)
{
  uint16_t c = 0;
  uint8_t n0 = (a >> 0) & 0x1;
  uint8_t n2 = (a >> 1) & 0x1;
  uint8_t n4 = (a >> 2) & 0x1;
  uint8_t n6 = (a >> 3) & 0x1;
  uint8_t n8 = (a >> 4) & 0x1;
  uint8_t n10 = (a >> 5) & 0x1;
  uint8_t n12 = (a >> 6) & 0x1;
  uint8_t n14 = (a >> 7) & 0x1;
  uint8_t n16 = (b >> 0) & 0x1;
  uint8_t n18 = (b >> 1) & 0x1;
  uint8_t n20 = (b >> 2) & 0x1;
  uint8_t n22 = (b >> 3) & 0x1;
  uint8_t n24 = (b >> 4) & 0x1;
  uint8_t n26 = (b >> 5) & 0x1;
  uint8_t n28 = (b >> 6) & 0x1;
  uint8_t n30 = (b >> 7) & 0x1;
  uint8_t n32;
  uint8_t n33;
  uint8_t n34;
  uint8_t n35;
  uint8_t n36;
  uint8_t n37;
  uint8_t n38;
  uint8_t n39;
  uint8_t n40;
  uint8_t n41;
  uint8_t n42;
  uint8_t n47;
  uint8_t n49;
  uint8_t n51;
  uint8_t n53;
  uint8_t n54;
  uint8_t n55;
  uint8_t n56;
  uint8_t n57;

  n32 = n0 ^ n16;
  n33 = n0 & n16;
  n34 = (n2 ^ n18) ^ n33;
  n35 = (n2 & n18) | (n18 & n33) | (n2 & n33);
  n36 = (n4 ^ n20) ^ n35;
  n37 = (n4 & n20) | (n20 & n35) | (n4 & n35);
  n38 = (n6 ^ n22) ^ n37;
  n39 = (n6 & n22) | (n22 & n37) | (n6 & n37);
  n40 = (n8 ^ n24) ^ n39;
  n41 = (n8 & n24) | (n24 & n39) | (n8 & n39);
  n42 = (n10 ^ n26) ^ n41;
  n47 = n4 & n20;
  n49 = (n6 & n22) | (n22 & n47) | (n6 & n47);
  n51 = (n8 & n24) | (n24 & n49) | (n8 & n49);
  n53 = (n10 & n26) | (n26 & n51) | (n10 & n51);
  n54 = (n12 ^ n28) ^ n53;
  n55 = (n12 & n28) | (n28 & n53) | (n12 & n53);
  n56 = (n14 ^ n30) ^ n55;
  n57 = (n14 & n30) | (n30 & n55) | (n14 & n55);

  c |= (n32 & 0x1) << 0;
  c |= (n34 & 0x1) << 1;
  c |= (n36 & 0x1) << 2;
  c |= (n38 & 0x1) << 3;
  c |= (n40 & 0x1) << 4;
  c |= (n42 & 0x1) << 5;
  c |= (n54 & 0x1) << 6;
  c |= (n56 & 0x1) << 7;
  c |= (n57 & 0x1) << 8;

  return c;
}

