// Library = EvoApprox8b
// Circuit = add8_GDA_S_N8_M8_P2
// Area   (180) = 2032
// Delay  (180) = 0.820
// Power  (180) = 607.80
// Area   (45) = 156
// Delay  (45) = 0.320
// Power  (45) = 55.93
// Nodes = 40
// HD = 36992
// MAE = 15.50000
// MSE = 1392.00000
// MRE = 7.07 %
// WCE = 144
// WCRE = 100 %
// EP = 30.1 %

module add8_GDA_St_N8_M8_P2(A, B, O);
  input [7:0] A;
  input [7:0] B;
  output [8:0] O;
  wire [2031:0] N;

  assign N[0] = A[0];
  assign N[1] = A[0];
  assign N[2] = A[1];
  assign N[3] = A[1];
  assign N[4] = A[2];
  assign N[5] = A[2];
  assign N[6] = A[3];
  assign N[7] = A[3];
  assign N[8] = A[4];
  assign N[9] = A[4];
  assign N[10] = A[5];
  assign N[11] = A[5];
  assign N[12] = A[6];
  assign N[13] = A[6];
  assign N[14] = A[7];
  assign N[15] = A[7];
  assign N[16] = B[0];
  assign N[17] = B[0];
  assign N[18] = B[1];
  assign N[19] = B[1];
  assign N[20] = B[2];
  assign N[21] = B[2];
  assign N[22] = B[3];
  assign N[23] = B[3];
  assign N[24] = B[4];
  assign N[25] = B[4];
  assign N[26] = B[5];
  assign N[27] = B[5];
  assign N[28] = B[6];
  assign N[29] = B[6];
  assign N[30] = B[7];
  assign N[31] = B[7];

  AND2X1 n32(.A(N[0]), .B(N[16]), .Y(N[32]));
  AND2X1 n34(.A(N[2]), .B(N[18]), .Y(N[34]));
  AND2X1 n36(.A(N[4]), .B(N[20]), .Y(N[36]));
  AND2X1 n38(.A(N[6]), .B(N[22]), .Y(N[38]));
  AND2X1 n40(.A(N[8]), .B(N[24]), .Y(N[40]));
  AND2X1 n42(.A(N[10]), .B(N[26]), .Y(N[42]));
  AND2X1 n44(.A(N[12]), .B(N[28]), .Y(N[44]));
  XOR2X1 n48(.A(N[2]), .B(N[18]), .Y(N[48]));
  XOR2X1 n50(.A(N[4]), .B(N[20]), .Y(N[50]));
  XOR2X1 n52(.A(N[6]), .B(N[22]), .Y(N[52]));
  XOR2X1 n54(.A(N[8]), .B(N[24]), .Y(N[54]));
  XOR2X1 n56(.A(N[10]), .B(N[26]), .Y(N[56]));
  XOR2X1 n58(.A(N[12]), .B(N[28]), .Y(N[58]));
  BUFX2 n60(.A(N[32]), .Y(N[60]));
  BUFX2 n62(.A(N[34]), .Y(N[62]));
  AND2X1 n64(.A(N[48]), .B(N[60]), .Y(N[64]));
  OR2X1 n66(.A(N[62]), .B(N[64]), .Y(N[66]));
  BUFX2 n68(.A(N[36]), .Y(N[68]));
  AND2X1 n70(.A(N[50]), .B(N[62]), .Y(N[70]));
  OR2X1 n72(.A(N[68]), .B(N[70]), .Y(N[72]));
  BUFX2 n74(.A(N[38]), .Y(N[74]));
  AND2X1 n76(.A(N[52]), .B(N[68]), .Y(N[76]));
  OR2X1 n78(.A(N[74]), .B(N[76]), .Y(N[78]));
  BUFX2 n80(.A(N[40]), .Y(N[80]));
  AND2X1 n82(.A(N[54]), .B(N[74]), .Y(N[82]));
  OR2X1 n84(.A(N[80]), .B(N[82]), .Y(N[84]));
  BUFX2 n86(.A(N[42]), .Y(N[86]));
  AND2X1 n88(.A(N[56]), .B(N[80]), .Y(N[88]));
  OR2X1 n90(.A(N[86]), .B(N[88]), .Y(N[90]));
  BUFX2 n92(.A(N[44]), .Y(N[92]));
  AND2X1 n94(.A(N[58]), .B(N[86]), .Y(N[94]));
  OR2X1 n96(.A(N[92]), .B(N[94]), .Y(N[96]));
  HAX1 n98(.A(N[0]), .B(N[16]), .YS(N[98]), .YC(N[99]));
  FAX1 n102(.A(N[2]), .B(N[18]), .C(N[60]), .YS(N[102]), .YC(N[103]));
  FAX1 n106(.A(N[4]), .B(N[20]), .C(N[66]), .YS(N[106]), .YC(N[107]));
  FAX1 n110(.A(N[6]), .B(N[22]), .C(N[72]), .YS(N[110]), .YC(N[111]));
  FAX1 n114(.A(N[8]), .B(N[24]), .C(N[78]), .YS(N[114]), .YC(N[115]));
  FAX1 n118(.A(N[10]), .B(N[26]), .C(N[84]), .YS(N[118]), .YC(N[119]));
  FAX1 n122(.A(N[12]), .B(N[28]), .C(N[90]), .YS(N[122]), .YC(N[123]));
  FAX1 n126(.A(N[14]), .B(N[30]), .C(N[96]), .YS(N[126]), .YC(N[127]));

  assign O[0] = N[98];
  assign O[1] = N[102];
  assign O[2] = N[106];
  assign O[3] = N[110];
  assign O[4] = N[114];
  assign O[5] = N[118];
  assign O[6] = N[122];
  assign O[7] = N[126];
  assign O[8] = N[127];

endmodule
