function [ c ] = add8_GDA_St_N8_M8_P3( a, b )
% Approximate function add8_GDA_St_N8_M8_P3
%  Library = EvoApprox8b
%  Circuit = add8_GDA_S_N8_M8_P3
%  Area   (180) = 2352
%  Delay  (180) = 1.050
%  Power  (180) = 721.90
%  Area   (45) = 180
%  Delay  (45) = 0.420
%  Power  (45) = 65.18
%  Nodes = 50
%  HD = 14464
%  MAE = 7.50000
%  MSE = 680.00000
%  MRE = 3.51 %
%  WCE = 128
%  WCRE = 100 %
%  EP = 12.5 %
  a = uint16(a);
  b = uint16(b);
  c = 0;
  n0 = bitand(bitshift(a, -0), 1, 'uint16');
  n2 = bitand(bitshift(a, -1), 1, 'uint16');
  n4 = bitand(bitshift(a, -2), 1, 'uint16');
  n6 = bitand(bitshift(a, -3), 1, 'uint16');
  n8 = bitand(bitshift(a, -4), 1, 'uint16');
  n10 = bitand(bitshift(a, -5), 1, 'uint16');
  n12 = bitand(bitshift(a, -6), 1, 'uint16');
  n14 = bitand(bitshift(a, -7), 1, 'uint16');
  n16 = bitand(bitshift(b, -0), 1, 'uint16');
  n18 = bitand(bitshift(b, -1), 1, 'uint16');
  n20 = bitand(bitshift(b, -2), 1, 'uint16');
  n22 = bitand(bitshift(b, -3), 1, 'uint16');
  n24 = bitand(bitshift(b, -4), 1, 'uint16');
  n26 = bitand(bitshift(b, -5), 1, 'uint16');
  n28 = bitand(bitshift(b, -6), 1, 'uint16');
  n30 = bitand(bitshift(b, -7), 1, 'uint16');
  n32 = bitand(n0, n16);
  n34 = bitand(n2, n18);
  n36 = bitand(n4, n20);
  n38 = bitand(n6, n22);
  n40 = bitand(n8, n24);
  n42 = bitand(n10, n26);
  n44 = bitand(n12, n28);
  n48 = bitxor(n2, n18);
  n50 = bitxor(n4, n20);
  n52 = bitxor(n6, n22);
  n54 = bitxor(n8, n24);
  n56 = bitxor(n10, n26);
  n58 = bitxor(n12, n28);
  n60 = n32;
  n62 = n34;
  n64 = bitand(n48, n60);
  n66 = bitor(n62, n64);
  n68 = n36;
  n70 = bitand(n50, n62);
  n72 = bitand(n50, n64);
  n74 = bitor(n70, n72);
  n76 = bitor(n68, n74);
  n78 = n38;
  n80 = bitand(n52, n68);
  n82 = bitand(n52, n70);
  n84 = bitor(n80, n82);
  n86 = bitor(n78, n84);
  n88 = n40;
  n90 = bitand(n54, n78);
  n92 = bitand(n54, n80);
  n94 = bitor(n90, n92);
  n96 = bitor(n88, n94);
  n98 = n42;
  n100 = bitand(n56, n88);
  n102 = bitand(n56, n90);
  n104 = bitor(n100, n102);
  n106 = bitor(n98, n104);
  n108 = n44;
  n110 = bitand(n58, n98);
  n112 = bitand(n58, n100);
  n114 = bitor(n110, n112);
  n116 = bitor(n108, n114);
  n118 = bitxor(n0, n16);
  n122 = bitxor(bitxor(n2, n18), n60);
  n126 = bitxor(bitxor(n4, n20), n66);
  n130 = bitxor(bitxor(n6, n22), n76);
  n134 = bitxor(bitxor(n8, n24), n86);
  n138 = bitxor(bitxor(n10, n26), n96);
  n142 = bitxor(bitxor(n12, n28), n106);
  n146 = bitxor(bitxor(n14, n30), n116);
  n147 = bitor(bitor(bitand(n14, n30), bitand(n30, n116)), bitand(n14, n116));
  c = bitor(c, bitshift(bitand(n118, 1), 0));
  c = bitor(c, bitshift(bitand(n122, 1), 1));
  c = bitor(c, bitshift(bitand(n126, 1), 2));
  c = bitor(c, bitshift(bitand(n130, 1), 3));
  c = bitor(c, bitshift(bitand(n134, 1), 4));
  c = bitor(c, bitshift(bitand(n138, 1), 5));
  c = bitor(c, bitshift(bitand(n142, 1), 6));
  c = bitor(c, bitshift(bitand(n146, 1), 7));
  c = bitor(c, bitshift(bitand(n147, 1), 8));
end