/// Approximate function add8_GDA_St_N8_M8_P3
///  Library = EvoApprox8b
///  Circuit = add8_GDA_S_N8_M8_P3
///  Area   (180) = 2352
///  Delay  (180) = 1.050
///  Power  (180) = 721.90
///  Area   (45) = 180
///  Delay  (45) = 0.420
///  Power  (45) = 65.18
///  Nodes = 50
///  HD = 14464
///  MAE = 7.50000
///  MSE = 680.00000
///  MRE = 3.51 %
///  WCE = 128
///  WCRE = 100 %
///  EP = 12.5 %
uint16_t add8_GDA_St_N8_M8_P3(uint8_t a, uint8_t b)
{
  uint16_t c = 0;
  uint8_t n0 = (a >> 0) & 0x1;
  uint8_t n2 = (a >> 1) & 0x1;
  uint8_t n4 = (a >> 2) & 0x1;
  uint8_t n6 = (a >> 3) & 0x1;
  uint8_t n8 = (a >> 4) & 0x1;
  uint8_t n10 = (a >> 5) & 0x1;
  uint8_t n12 = (a >> 6) & 0x1;
  uint8_t n14 = (a >> 7) & 0x1;
  uint8_t n16 = (b >> 0) & 0x1;
  uint8_t n18 = (b >> 1) & 0x1;
  uint8_t n20 = (b >> 2) & 0x1;
  uint8_t n22 = (b >> 3) & 0x1;
  uint8_t n24 = (b >> 4) & 0x1;
  uint8_t n26 = (b >> 5) & 0x1;
  uint8_t n28 = (b >> 6) & 0x1;
  uint8_t n30 = (b >> 7) & 0x1;
  uint8_t n32;
  uint8_t n34;
  uint8_t n36;
  uint8_t n38;
  uint8_t n40;
  uint8_t n42;
  uint8_t n44;
  uint8_t n48;
  uint8_t n50;
  uint8_t n52;
  uint8_t n54;
  uint8_t n56;
  uint8_t n58;
  uint8_t n60;
  uint8_t n62;
  uint8_t n64;
  uint8_t n66;
  uint8_t n68;
  uint8_t n70;
  uint8_t n72;
  uint8_t n74;
  uint8_t n76;
  uint8_t n78;
  uint8_t n80;
  uint8_t n82;
  uint8_t n84;
  uint8_t n86;
  uint8_t n88;
  uint8_t n90;
  uint8_t n92;
  uint8_t n94;
  uint8_t n96;
  uint8_t n98;
  uint8_t n100;
  uint8_t n102;
  uint8_t n104;
  uint8_t n106;
  uint8_t n108;
  uint8_t n110;
  uint8_t n112;
  uint8_t n114;
  uint8_t n116;
  uint8_t n118;
  uint8_t n122;
  uint8_t n126;
  uint8_t n130;
  uint8_t n134;
  uint8_t n138;
  uint8_t n142;
  uint8_t n146;
  uint8_t n147;

  n32 = n0 & n16;
  n34 = n2 & n18;
  n36 = n4 & n20;
  n38 = n6 & n22;
  n40 = n8 & n24;
  n42 = n10 & n26;
  n44 = n12 & n28;
  n48 = n2 ^ n18;
  n50 = n4 ^ n20;
  n52 = n6 ^ n22;
  n54 = n8 ^ n24;
  n56 = n10 ^ n26;
  n58 = n12 ^ n28;
  n60 = n32;
  n62 = n34;
  n64 = n48 & n60;
  n66 = n62 | n64;
  n68 = n36;
  n70 = n50 & n62;
  n72 = n50 & n64;
  n74 = n70 | n72;
  n76 = n68 | n74;
  n78 = n38;
  n80 = n52 & n68;
  n82 = n52 & n70;
  n84 = n80 | n82;
  n86 = n78 | n84;
  n88 = n40;
  n90 = n54 & n78;
  n92 = n54 & n80;
  n94 = n90 | n92;
  n96 = n88 | n94;
  n98 = n42;
  n100 = n56 & n88;
  n102 = n56 & n90;
  n104 = n100 | n102;
  n106 = n98 | n104;
  n108 = n44;
  n110 = n58 & n98;
  n112 = n58 & n100;
  n114 = n110 | n112;
  n116 = n108 | n114;
  n118 = n0 ^ n16;
  n122 = (n2 ^ n18) ^ n60;
  n126 = (n4 ^ n20) ^ n66;
  n130 = (n6 ^ n22) ^ n76;
  n134 = (n8 ^ n24) ^ n86;
  n138 = (n10 ^ n26) ^ n96;
  n142 = (n12 ^ n28) ^ n106;
  n146 = (n14 ^ n30) ^ n116;
  n147 = (n14 & n30) | (n30 & n116) | (n14 & n116);

  c |= (n118 & 0x1) << 0;
  c |= (n122 & 0x1) << 1;
  c |= (n126 & 0x1) << 2;
  c |= (n130 & 0x1) << 3;
  c |= (n134 & 0x1) << 4;
  c |= (n138 & 0x1) << 5;
  c |= (n142 & 0x1) << 6;
  c |= (n146 & 0x1) << 7;
  c |= (n147 & 0x1) << 8;

  return c;
}

