/// Approximate function add8_GeAr_N8_R1_P3
///  Library = EvoApprox8b
///  Circuit = add8_GA_N8_R1_P3
///  Area   (180) = 2200
///  Delay  (180) = 1.210
///  Power  (180) = 748.50
///  Area   (45) = 157
///  Delay  (45) = 0.420
///  Power  (45) = 72.57
///  Nodes = 20
///  HD = 14464
///  MAE = 7.50000
///  MSE = 680.00000
///  MRE = 3.51 %
///  WCE = 128
///  WCRE = 100 %
///  EP = 12.5 %
uint16_t add8_GeAr_N8_R1_P3(uint8_t a, uint8_t b)
{
  uint16_t c = 0;
  uint8_t n0 = (a >> 0) & 0x1;
  uint8_t n2 = (a >> 1) & 0x1;
  uint8_t n4 = (a >> 2) & 0x1;
  uint8_t n6 = (a >> 3) & 0x1;
  uint8_t n8 = (a >> 4) & 0x1;
  uint8_t n10 = (a >> 5) & 0x1;
  uint8_t n12 = (a >> 6) & 0x1;
  uint8_t n14 = (a >> 7) & 0x1;
  uint8_t n16 = (b >> 0) & 0x1;
  uint8_t n18 = (b >> 1) & 0x1;
  uint8_t n20 = (b >> 2) & 0x1;
  uint8_t n22 = (b >> 3) & 0x1;
  uint8_t n24 = (b >> 4) & 0x1;
  uint8_t n26 = (b >> 5) & 0x1;
  uint8_t n28 = (b >> 6) & 0x1;
  uint8_t n30 = (b >> 7) & 0x1;
  uint8_t n32;
  uint8_t n33;
  uint8_t n34;
  uint8_t n35;
  uint8_t n36;
  uint8_t n37;
  uint8_t n38;
  uint8_t n43;
  uint8_t n45;
  uint8_t n47;
  uint8_t n48;
  uint8_t n53;
  uint8_t n55;
  uint8_t n57;
  uint8_t n58;
  uint8_t n63;
  uint8_t n65;
  uint8_t n67;
  uint8_t n68;
  uint8_t n73;
  uint8_t n75;
  uint8_t n77;
  uint8_t n78;
  uint8_t n79;

  n32 = n0 ^ n16;
  n33 = n0 & n16;
  n34 = (n2 ^ n18) ^ n33;
  n35 = (n2 & n18) | (n18 & n33) | (n2 & n33);
  n36 = (n4 ^ n20) ^ n35;
  n37 = (n4 & n20) | (n20 & n35) | (n4 & n35);
  n38 = (n6 ^ n22) ^ n37;
  n43 = n2 & n18;
  n45 = (n4 & n20) | (n20 & n43) | (n4 & n43);
  n47 = (n6 & n22) | (n22 & n45) | (n6 & n45);
  n48 = (n8 ^ n24) ^ n47;
  n53 = n4 & n20;
  n55 = (n6 & n22) | (n22 & n53) | (n6 & n53);
  n57 = (n8 & n24) | (n24 & n55) | (n8 & n55);
  n58 = (n10 ^ n26) ^ n57;
  n63 = n6 & n22;
  n65 = (n8 & n24) | (n24 & n63) | (n8 & n63);
  n67 = (n10 & n26) | (n26 & n65) | (n10 & n65);
  n68 = (n12 ^ n28) ^ n67;
  n73 = n8 & n24;
  n75 = (n10 & n26) | (n26 & n73) | (n10 & n73);
  n77 = (n12 & n28) | (n28 & n75) | (n12 & n75);
  n78 = (n14 ^ n30) ^ n77;
  n79 = (n14 & n30) | (n30 & n77) | (n14 & n77);

  c |= (n32 & 0x1) << 0;
  c |= (n34 & 0x1) << 1;
  c |= (n36 & 0x1) << 2;
  c |= (n38 & 0x1) << 3;
  c |= (n48 & 0x1) << 4;
  c |= (n58 & 0x1) << 5;
  c |= (n68 & 0x1) << 6;
  c |= (n78 & 0x1) << 7;
  c |= (n79 & 0x1) << 8;

  return c;
}

