/// Approximate function add8_GeAr_N8_R1_P1
///  Library = EvoApprox8b
///  Circuit = add8_GA_N8_R1_P1
///  Area   (180) = 1400
///  Delay  (180) = 0.530
///  Power  (180) = 364.20
///  Area   (45) = 95
///  Delay  (45) = 0.190
///  Power  (45) = 36.08
///  Nodes = 14
///  HD = 90240
///  MAE = 31.50000
///  MSE = 3040.00000
///  MRE = 13.69 %
///  WCE = 168
///  WCRE = 100 %
///  EP = 60.2 %
uint16_t add8_GeAr_N8_R1_P1(uint8_t a, uint8_t b)
{
  uint16_t c = 0;
  uint8_t n0 = (a >> 0) & 0x1;
  uint8_t n2 = (a >> 1) & 0x1;
  uint8_t n4 = (a >> 2) & 0x1;
  uint8_t n6 = (a >> 3) & 0x1;
  uint8_t n8 = (a >> 4) & 0x1;
  uint8_t n10 = (a >> 5) & 0x1;
  uint8_t n12 = (a >> 6) & 0x1;
  uint8_t n14 = (a >> 7) & 0x1;
  uint8_t n16 = (b >> 0) & 0x1;
  uint8_t n18 = (b >> 1) & 0x1;
  uint8_t n20 = (b >> 2) & 0x1;
  uint8_t n22 = (b >> 3) & 0x1;
  uint8_t n24 = (b >> 4) & 0x1;
  uint8_t n26 = (b >> 5) & 0x1;
  uint8_t n28 = (b >> 6) & 0x1;
  uint8_t n30 = (b >> 7) & 0x1;
  uint8_t n32;
  uint8_t n33;
  uint8_t n34;
  uint8_t n39;
  uint8_t n40;
  uint8_t n45;
  uint8_t n46;
  uint8_t n51;
  uint8_t n52;
  uint8_t n57;
  uint8_t n58;
  uint8_t n63;
  uint8_t n64;
  uint8_t n69;
  uint8_t n70;
  uint8_t n71;

  n32 = n0 ^ n16;
  n33 = n0 & n16;
  n34 = (n2 ^ n18) ^ n33;
  n39 = n2 & n18;
  n40 = (n4 ^ n20) ^ n39;
  n45 = n4 & n20;
  n46 = (n6 ^ n22) ^ n45;
  n51 = n6 & n22;
  n52 = (n8 ^ n24) ^ n51;
  n57 = n8 & n24;
  n58 = (n10 ^ n26) ^ n57;
  n63 = n10 & n26;
  n64 = (n12 ^ n28) ^ n63;
  n69 = n12 & n28;
  n70 = (n14 ^ n30) ^ n69;
  n71 = (n14 & n30) | (n30 & n69) | (n14 & n69);

  c |= (n32 & 0x1) << 0;
  c |= (n34 & 0x1) << 1;
  c |= (n40 & 0x1) << 2;
  c |= (n46 & 0x1) << 3;
  c |= (n52 & 0x1) << 4;
  c |= (n58 & 0x1) << 5;
  c |= (n64 & 0x1) << 6;
  c |= (n70 & 0x1) << 7;
  c |= (n71 & 0x1) << 8;

  return c;
}

