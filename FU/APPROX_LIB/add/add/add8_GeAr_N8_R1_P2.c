/// Approximate function add8_GeAr_N8_R1_P2
///  Library = EvoApprox8b
///  Circuit = add8_GA_N8_R1_P2
///  Area   (180) = 1920
///  Delay  (180) = 0.870
///  Power  (180) = 593.20
///  Area   (45) = 135
///  Delay  (45) = 0.310
///  Power  (45) = 58.15
///  Nodes = 18
///  HD = 36992
///  MAE = 15.50000
///  MSE = 1392.00000
///  MRE = 7.07 %
///  WCE = 144
///  WCRE = 100 %
///  EP = 30.1 %
uint16_t add8_GeAr_N8_R1_P2(uint8_t a, uint8_t b)
{
  uint16_t c = 0;
  uint8_t n0 = (a >> 0) & 0x1;
  uint8_t n2 = (a >> 1) & 0x1;
  uint8_t n4 = (a >> 2) & 0x1;
  uint8_t n6 = (a >> 3) & 0x1;
  uint8_t n8 = (a >> 4) & 0x1;
  uint8_t n10 = (a >> 5) & 0x1;
  uint8_t n12 = (a >> 6) & 0x1;
  uint8_t n14 = (a >> 7) & 0x1;
  uint8_t n16 = (b >> 0) & 0x1;
  uint8_t n18 = (b >> 1) & 0x1;
  uint8_t n20 = (b >> 2) & 0x1;
  uint8_t n22 = (b >> 3) & 0x1;
  uint8_t n24 = (b >> 4) & 0x1;
  uint8_t n26 = (b >> 5) & 0x1;
  uint8_t n28 = (b >> 6) & 0x1;
  uint8_t n30 = (b >> 7) & 0x1;
  uint8_t n32;
  uint8_t n33;
  uint8_t n34;
  uint8_t n35;
  uint8_t n36;
  uint8_t n41;
  uint8_t n43;
  uint8_t n44;
  uint8_t n49;
  uint8_t n51;
  uint8_t n52;
  uint8_t n57;
  uint8_t n59;
  uint8_t n60;
  uint8_t n65;
  uint8_t n67;
  uint8_t n68;
  uint8_t n73;
  uint8_t n75;
  uint8_t n76;
  uint8_t n77;

  n32 = n0 ^ n16;
  n33 = n0 & n16;
  n34 = (n2 ^ n18) ^ n33;
  n35 = (n2 & n18) | (n18 & n33) | (n2 & n33);
  n36 = (n4 ^ n20) ^ n35;
  n41 = n2 & n18;
  n43 = (n4 & n20) | (n20 & n41) | (n4 & n41);
  n44 = (n6 ^ n22) ^ n43;
  n49 = n4 & n20;
  n51 = (n6 & n22) | (n22 & n49) | (n6 & n49);
  n52 = (n8 ^ n24) ^ n51;
  n57 = n6 & n22;
  n59 = (n8 & n24) | (n24 & n57) | (n8 & n57);
  n60 = (n10 ^ n26) ^ n59;
  n65 = n8 & n24;
  n67 = (n10 & n26) | (n26 & n65) | (n10 & n65);
  n68 = (n12 ^ n28) ^ n67;
  n73 = n10 & n26;
  n75 = (n12 & n28) | (n28 & n73) | (n12 & n73);
  n76 = (n14 ^ n30) ^ n75;
  n77 = (n14 & n30) | (n30 & n75) | (n14 & n75);

  c |= (n32 & 0x1) << 0;
  c |= (n34 & 0x1) << 1;
  c |= (n36 & 0x1) << 2;
  c |= (n44 & 0x1) << 3;
  c |= (n52 & 0x1) << 4;
  c |= (n60 & 0x1) << 5;
  c |= (n68 & 0x1) << 6;
  c |= (n76 & 0x1) << 7;
  c |= (n77 & 0x1) << 8;

  return c;
}

