/// Approximate function add8_GDA_St_N8_M8_P1
///  Library = EvoApprox8b
///  Circuit = add8_GDA_S_N8_M8_P1
///  Area   (180) = 1312
///  Delay  (180) = 0.510
///  Power  (180) = 338.40
///  Area   (45) = 100
///  Delay  (45) = 0.200
///  Power  (45) = 32.50
///  Nodes = 22
///  HD = 90240
///  MAE = 31.50000
///  MSE = 3040.00000
///  MRE = 13.69 %
///  WCE = 168
///  WCRE = 100 %
///  EP = 60.2 %
uint16_t add8_GDA_St_N8_M8_P1(uint8_t a, uint8_t b)
{
  uint16_t c = 0;
  uint8_t n0 = (a >> 0) & 0x1;
  uint8_t n2 = (a >> 1) & 0x1;
  uint8_t n4 = (a >> 2) & 0x1;
  uint8_t n6 = (a >> 3) & 0x1;
  uint8_t n8 = (a >> 4) & 0x1;
  uint8_t n10 = (a >> 5) & 0x1;
  uint8_t n12 = (a >> 6) & 0x1;
  uint8_t n14 = (a >> 7) & 0x1;
  uint8_t n16 = (b >> 0) & 0x1;
  uint8_t n18 = (b >> 1) & 0x1;
  uint8_t n20 = (b >> 2) & 0x1;
  uint8_t n22 = (b >> 3) & 0x1;
  uint8_t n24 = (b >> 4) & 0x1;
  uint8_t n26 = (b >> 5) & 0x1;
  uint8_t n28 = (b >> 6) & 0x1;
  uint8_t n30 = (b >> 7) & 0x1;
  uint8_t n32;
  uint8_t n34;
  uint8_t n36;
  uint8_t n38;
  uint8_t n40;
  uint8_t n42;
  uint8_t n44;
  uint8_t n60;
  uint8_t n62;
  uint8_t n64;
  uint8_t n66;
  uint8_t n68;
  uint8_t n70;
  uint8_t n72;
  uint8_t n74;
  uint8_t n78;
  uint8_t n82;
  uint8_t n86;
  uint8_t n90;
  uint8_t n94;
  uint8_t n98;
  uint8_t n102;
  uint8_t n103;

  n32 = n0 & n16;
  n34 = n2 & n18;
  n36 = n4 & n20;
  n38 = n6 & n22;
  n40 = n8 & n24;
  n42 = n10 & n26;
  n44 = n12 & n28;
  n60 = n32;
  n62 = n34;
  n64 = n36;
  n66 = n38;
  n68 = n40;
  n70 = n42;
  n72 = n44;
  n74 = n0 ^ n16;
  n78 = (n2 ^ n18) ^ n60;
  n82 = (n4 ^ n20) ^ n62;
  n86 = (n6 ^ n22) ^ n64;
  n90 = (n8 ^ n24) ^ n66;
  n94 = (n10 ^ n26) ^ n68;
  n98 = (n12 ^ n28) ^ n70;
  n102 = (n14 ^ n30) ^ n72;
  n103 = (n14 & n30) | (n30 & n72) | (n14 & n72);

  c |= (n74 & 0x1) << 0;
  c |= (n78 & 0x1) << 1;
  c |= (n82 & 0x1) << 2;
  c |= (n86 & 0x1) << 3;
  c |= (n90 & 0x1) << 4;
  c |= (n94 & 0x1) << 5;
  c |= (n98 & 0x1) << 6;
  c |= (n102 & 0x1) << 7;
  c |= (n103 & 0x1) << 8;

  return c;
}

