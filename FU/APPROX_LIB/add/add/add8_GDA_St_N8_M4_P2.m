function [ c ] = add8_GDA_St_N8_M4_P2( a, b )
% Approximate function add8_GDA_St_N8_M4_P2
%  Library = EvoApprox8b
%  Circuit = add8_GDA_S_N8_M4_P2
%  Area   (180) = 1472
%  Delay  (180) = 0.950
%  Power  (180) = 458.30
%  Area   (45) = 109
%  Delay  (45) = 0.360
%  Power  (45) = 43.46
%  Nodes = 23
%  HD = 22656
%  MAE = 7.50000
%  MSE = 408.00000
%  MRE = 3.65 %
%  WCE = 64
%  WCRE = 100 %
%  EP = 18.8 %
  a = uint16(a);
  b = uint16(b);
  c = 0;
  n0 = bitand(bitshift(a, -0), 1, 'uint16');
  n2 = bitand(bitshift(a, -1), 1, 'uint16');
  n4 = bitand(bitshift(a, -2), 1, 'uint16');
  n6 = bitand(bitshift(a, -3), 1, 'uint16');
  n8 = bitand(bitshift(a, -4), 1, 'uint16');
  n10 = bitand(bitshift(a, -5), 1, 'uint16');
  n12 = bitand(bitshift(a, -6), 1, 'uint16');
  n14 = bitand(bitshift(a, -7), 1, 'uint16');
  n16 = bitand(bitshift(b, -0), 1, 'uint16');
  n18 = bitand(bitshift(b, -1), 1, 'uint16');
  n20 = bitand(bitshift(b, -2), 1, 'uint16');
  n22 = bitand(bitshift(b, -3), 1, 'uint16');
  n24 = bitand(bitshift(b, -4), 1, 'uint16');
  n26 = bitand(bitshift(b, -5), 1, 'uint16');
  n28 = bitand(bitshift(b, -6), 1, 'uint16');
  n30 = bitand(bitshift(b, -7), 1, 'uint16');
  n32 = bitand(n0, n16);
  n34 = bitand(n2, n18);
  n38 = bitxor(n2, n18);
  n40 = bitand(n38, n32);
  n42 = bitor(n34, n40);
  n44 = bitand(n4, n20);
  n46 = bitand(n6, n22);
  n50 = bitxor(n6, n22);
  n52 = bitand(n50, n44);
  n54 = bitor(n46, n52);
  n56 = bitand(n8, n24);
  n58 = bitand(n10, n26);
  n62 = bitxor(n10, n26);
  n64 = bitand(n62, n56);
  n66 = bitor(n58, n64);
  n68 = bitxor(n0, n16);
  n69 = bitand(n0, n16);
  n70 = bitxor(bitxor(n2, n18), n69);
  n74 = bitxor(bitxor(n4, n20), n42);
  n75 = bitor(bitor(bitand(n4, n20), bitand(n20, n42)), bitand(n4, n42));
  n76 = bitxor(bitxor(n6, n22), n75);
  n80 = bitxor(bitxor(n8, n24), n54);
  n81 = bitor(bitor(bitand(n8, n24), bitand(n24, n54)), bitand(n8, n54));
  n82 = bitxor(bitxor(n10, n26), n81);
  n86 = bitxor(bitxor(n12, n28), n66);
  n87 = bitor(bitor(bitand(n12, n28), bitand(n28, n66)), bitand(n12, n66));
  n88 = bitxor(bitxor(n14, n30), n87);
  n89 = bitor(bitor(bitand(n14, n30), bitand(n30, n87)), bitand(n14, n87));
  c = bitor(c, bitshift(bitand(n68, 1), 0));
  c = bitor(c, bitshift(bitand(n70, 1), 1));
  c = bitor(c, bitshift(bitand(n74, 1), 2));
  c = bitor(c, bitshift(bitand(n76, 1), 3));
  c = bitor(c, bitshift(bitand(n80, 1), 4));
  c = bitor(c, bitshift(bitand(n82, 1), 5));
  c = bitor(c, bitshift(bitand(n86, 1), 6));
  c = bitor(c, bitshift(bitand(n88, 1), 7));
  c = bitor(c, bitshift(bitand(n89, 1), 8));
end