/// Approximate function add8_ACA_II_N8_Q4
///  Library = EvoApprox8b
///  Circuit = add8_ACA_II_N8_Q4
///  Area   (180) = 1320
///  Delay  (180) = 0.950
///  Power  (180) = 431.50
///  Area   (45) = 94
///  Delay  (45) = 0.340
///  Power  (45) = 42.26
///  Nodes = 12
///  HD = 22656
///  MAE = 7.50000
///  MSE = 408.00000
///  MRE = 3.65 %
///  WCE = 64
///  WCRE = 100 %
///  EP = 18.8 %
uint16_t add8_ACA_II_N8_Q4(uint8_t a, uint8_t b)
{
  uint16_t c = 0;
  uint8_t n0 = (a >> 0) & 0x1;
  uint8_t n2 = (a >> 1) & 0x1;
  uint8_t n4 = (a >> 2) & 0x1;
  uint8_t n6 = (a >> 3) & 0x1;
  uint8_t n8 = (a >> 4) & 0x1;
  uint8_t n10 = (a >> 5) & 0x1;
  uint8_t n12 = (a >> 6) & 0x1;
  uint8_t n14 = (a >> 7) & 0x1;
  uint8_t n16 = (b >> 0) & 0x1;
  uint8_t n18 = (b >> 1) & 0x1;
  uint8_t n20 = (b >> 2) & 0x1;
  uint8_t n22 = (b >> 3) & 0x1;
  uint8_t n24 = (b >> 4) & 0x1;
  uint8_t n26 = (b >> 5) & 0x1;
  uint8_t n28 = (b >> 6) & 0x1;
  uint8_t n30 = (b >> 7) & 0x1;
  uint8_t n32;
  uint8_t n33;
  uint8_t n34;
  uint8_t n35;
  uint8_t n36;
  uint8_t n37;
  uint8_t n38;
  uint8_t n43;
  uint8_t n45;
  uint8_t n46;
  uint8_t n47;
  uint8_t n48;
  uint8_t n53;
  uint8_t n55;
  uint8_t n56;
  uint8_t n57;
  uint8_t n58;
  uint8_t n59;

  n32 = n0 ^ n16;
  n33 = n0 & n16;
  n34 = (n2 ^ n18) ^ n33;
  n35 = (n2 & n18) | (n18 & n33) | (n2 & n33);
  n36 = (n4 ^ n20) ^ n35;
  n37 = (n4 & n20) | (n20 & n35) | (n4 & n35);
  n38 = (n6 ^ n22) ^ n37;
  n43 = n4 & n20;
  n45 = (n6 & n22) | (n22 & n43) | (n6 & n43);
  n46 = (n8 ^ n24) ^ n45;
  n47 = (n8 & n24) | (n24 & n45) | (n8 & n45);
  n48 = (n10 ^ n26) ^ n47;
  n53 = n8 & n24;
  n55 = (n10 & n26) | (n26 & n53) | (n10 & n53);
  n56 = (n12 ^ n28) ^ n55;
  n57 = (n12 & n28) | (n28 & n55) | (n12 & n55);
  n58 = (n14 ^ n30) ^ n57;
  n59 = (n14 & n30) | (n30 & n57) | (n14 & n57);

  c |= (n32 & 0x1) << 0;
  c |= (n34 & 0x1) << 1;
  c |= (n36 & 0x1) << 2;
  c |= (n38 & 0x1) << 3;
  c |= (n46 & 0x1) << 4;
  c |= (n48 & 0x1) << 5;
  c |= (n56 & 0x1) << 6;
  c |= (n58 & 0x1) << 7;
  c |= (n59 & 0x1) << 8;

  return c;
}

