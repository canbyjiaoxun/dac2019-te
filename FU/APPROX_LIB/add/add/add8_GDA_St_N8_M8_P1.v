// Library = EvoApprox8b
// Circuit = add8_GDA_S_N8_M8_P1
// Area   (180) = 1312
// Delay  (180) = 0.510
// Power  (180) = 338.40
// Area   (45) = 100
// Delay  (45) = 0.200
// Power  (45) = 32.50
// Nodes = 22
// HD = 90240
// MAE = 31.50000
// MSE = 3040.00000
// MRE = 13.69 %
// WCE = 168
// WCRE = 100 %
// EP = 60.2 %

module add8_GDA_St_N8_M8_P1(A, B, O);
  input [7:0] A;
  input [7:0] B;
  output [8:0] O;
  wire [2031:0] N;

  assign N[0] = A[0];
  assign N[1] = A[0];
  assign N[2] = A[1];
  assign N[3] = A[1];
  assign N[4] = A[2];
  assign N[5] = A[2];
  assign N[6] = A[3];
  assign N[7] = A[3];
  assign N[8] = A[4];
  assign N[9] = A[4];
  assign N[10] = A[5];
  assign N[11] = A[5];
  assign N[12] = A[6];
  assign N[13] = A[6];
  assign N[14] = A[7];
  assign N[15] = A[7];
  assign N[16] = B[0];
  assign N[17] = B[0];
  assign N[18] = B[1];
  assign N[19] = B[1];
  assign N[20] = B[2];
  assign N[21] = B[2];
  assign N[22] = B[3];
  assign N[23] = B[3];
  assign N[24] = B[4];
  assign N[25] = B[4];
  assign N[26] = B[5];
  assign N[27] = B[5];
  assign N[28] = B[6];
  assign N[29] = B[6];
  assign N[30] = B[7];
  assign N[31] = B[7];

  AND2X1 n32(.A(N[0]), .B(N[16]), .Y(N[32]));
  AND2X1 n34(.A(N[2]), .B(N[18]), .Y(N[34]));
  AND2X1 n36(.A(N[4]), .B(N[20]), .Y(N[36]));
  AND2X1 n38(.A(N[6]), .B(N[22]), .Y(N[38]));
  AND2X1 n40(.A(N[8]), .B(N[24]), .Y(N[40]));
  AND2X1 n42(.A(N[10]), .B(N[26]), .Y(N[42]));
  AND2X1 n44(.A(N[12]), .B(N[28]), .Y(N[44]));
  BUFX2 n60(.A(N[32]), .Y(N[60]));
  BUFX2 n62(.A(N[34]), .Y(N[62]));
  BUFX2 n64(.A(N[36]), .Y(N[64]));
  BUFX2 n66(.A(N[38]), .Y(N[66]));
  BUFX2 n68(.A(N[40]), .Y(N[68]));
  BUFX2 n70(.A(N[42]), .Y(N[70]));
  BUFX2 n72(.A(N[44]), .Y(N[72]));
  HAX1 n74(.A(N[0]), .B(N[16]), .YS(N[74]), .YC(N[75]));
  FAX1 n78(.A(N[2]), .B(N[18]), .C(N[60]), .YS(N[78]), .YC(N[79]));
  FAX1 n82(.A(N[4]), .B(N[20]), .C(N[62]), .YS(N[82]), .YC(N[83]));
  FAX1 n86(.A(N[6]), .B(N[22]), .C(N[64]), .YS(N[86]), .YC(N[87]));
  FAX1 n90(.A(N[8]), .B(N[24]), .C(N[66]), .YS(N[90]), .YC(N[91]));
  FAX1 n94(.A(N[10]), .B(N[26]), .C(N[68]), .YS(N[94]), .YC(N[95]));
  FAX1 n98(.A(N[12]), .B(N[28]), .C(N[70]), .YS(N[98]), .YC(N[99]));
  FAX1 n102(.A(N[14]), .B(N[30]), .C(N[72]), .YS(N[102]), .YC(N[103]));

  assign O[0] = N[74];
  assign O[1] = N[78];
  assign O[2] = N[82];
  assign O[3] = N[86];
  assign O[4] = N[90];
  assign O[5] = N[94];
  assign O[6] = N[98];
  assign O[7] = N[102];
  assign O[8] = N[103];

endmodule
