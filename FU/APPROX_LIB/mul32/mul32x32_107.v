// Library = EvoApprox 16x16
// Circuit = mul32x32_107
// Area   (45) = 322
// Delay  (45) = 1.230
// Power  (45) = 0.13
// MAE = 318483583594199872.0
// MRE = 644.20 %
// WCE = 919959138623076736
// WCE% = 4.987 %
// WCE LIMIT = 1844674407370955264
// WCE LIMIT % = 10 %
// EP = 100.0 %
module mul32x32_107 (
A,
B,
Z
);

input [31:0] A;
input [31:0] B;
output [63:0] Z;

wire sig_85;
wire sig_86;
wire sig_92;
wire sig_158;
wire sig_197;
wire sig_207;
wire sig_211;
wire sig_224;
wire sig_268;
wire sig_280;
wire sig_365;
wire sig_435;
wire sig_481;
wire sig_504;
wire sig_654;
wire sig_683;
wire sig_735;
wire sig_810;
wire sig_827;
wire sig_966;
wire sig_997;
wire sig_1015;
wire sig_1041;
wire sig_1058;
wire sig_1087;
wire sig_1097;
wire sig_1208;
wire sig_1219;
wire sig_1245;
wire sig_1311;
wire sig_1361;
wire sig_1395;
wire sig_1405;
wire sig_1641;
wire sig_1779;
wire sig_1931;
wire sig_1973;
wire sig_2009;
wire sig_2079;
wire sig_2089;
wire sig_2150;
wire sig_2176;
wire sig_2421;
wire sig_2424;
wire sig_2440;
wire sig_2485;
wire sig_2504;
wire sig_2545;
wire sig_2643;
wire sig_2740;
wire sig_2805;
wire sig_2936;
wire sig_2965;
wire sig_3002;
wire sig_3008;
wire sig_3267;
wire sig_3272;
wire sig_3284;
wire sig_3321;
wire sig_3344;
wire sig_3352;
wire sig_3383;
wire sig_3438;
wire sig_3452;
wire sig_3475;
wire sig_3476;
wire sig_3613;
wire sig_3628;
wire sig_3647;
wire sig_3671;
wire sig_3718;
wire sig_3734;
wire sig_3737;
wire sig_3748;
wire sig_3760;
wire sig_3764;
wire sig_3849;
wire sig_3862;
wire sig_3953;
wire sig_3974;
wire sig_3992;
wire sig_4019;
wire sig_4087;
wire sig_4191;
wire sig_4230;
wire sig_4402;
wire sig_4446;
wire sig_4461;
wire sig_4494;
wire sig_4511;
wire sig_4516;
wire sig_4532;
wire sig_4643;
wire sig_4671;
wire sig_4696;
wire sig_4738;
wire sig_4834;
wire sig_4880;
wire sig_4902;
wire sig_4905;
wire sig_4915;
wire sig_4954;
wire sig_4966;
wire sig_4985;
wire sig_5057;
wire sig_5080;
wire sig_5103;
wire sig_5112;
wire sig_5142;
wire sig_5149;
wire sig_5153;
wire sig_5182;
wire sig_5193;
wire sig_5215;
wire sig_5249;
wire sig_5250;
wire sig_5268;
wire sig_5310;
wire sig_5314;
wire sig_5374;
wire sig_5377;
wire sig_5391;
wire sig_5393;
wire sig_5437;
wire sig_5458;
wire sig_5460;
wire sig_5514;
wire sig_5518;
wire sig_5548;
wire sig_5554;
wire sig_5555;
wire sig_5622;
wire sig_6110;
wire sig_6323;
wire sig_6327;
wire sig_6329;
wire sig_6330;
wire sig_6331;
wire sig_6332;
wire sig_6333;
wire sig_6334;
wire sig_6335;
wire sig_6336;
wire sig_6337;
wire sig_6338;
wire sig_6339;
wire sig_6340;

assign sig_85 = ~ A[8];
assign sig_86 = ~ B[15];
assign sig_92 = sig_86 ^ sig_86;
assign sig_158 = A[28] & B[31];
assign sig_197 = A[26];
assign sig_207 = ~ A[8];
assign sig_211 = ~ (A[8] | sig_85);
assign sig_224 = ~ sig_92;
assign sig_268 = A[31] & B[27];
assign sig_280 = A[30] & B[30];
assign sig_365 = A[28] & B[30];
assign sig_435 = A[29] & B[29];
assign sig_481 = A[29] & B[31];
assign sig_504 = A[31] & B[28];
assign sig_654 = A[29];
assign sig_683 = B[28];
assign sig_735 = A[27] & B[31];
assign sig_810 = A[31] & B[31];
assign sig_827 = A[31] & B[29];
assign sig_966 = A[30] & B[28];
assign sig_997 = A[29] & B[30];
assign sig_1015 = A[31] & B[30];
assign sig_1041 = ~ sig_224;
assign sig_1058 = A[30] & B[29];
assign sig_1087 = ~ sig_207;
assign sig_1097 = A[31];
assign sig_1208 = B[29] & B[26];
assign sig_1219 = A[30] & B[31];
assign sig_1245 = sig_268 ^ sig_966;
assign sig_1311 = A[27] & B[30];
assign sig_1361 = sig_1208 & sig_1097;
assign sig_1395 = sig_268 & sig_966;
assign sig_1405 = sig_504 ^ sig_1058;
assign sig_1641 = sig_683 & sig_654;
assign sig_1779 = sig_435 & sig_1245;
assign sig_1931 = sig_1245 ^ sig_435;
assign sig_1973 = sig_1395 | sig_1779;
assign sig_2009 = sig_1405 ^ sig_1973;
assign sig_2079 = sig_1361;
assign sig_2089 = sig_1058 & sig_504;
assign sig_2150 = ~ sig_1779;
assign sig_2176 = sig_1641;
assign sig_2421 = sig_1405 & sig_1973;
assign sig_2424 = sig_2009 ^ sig_997;
assign sig_2440 = sig_1931 ^ sig_2176;
assign sig_2485 = sig_2440 ^ sig_365;
assign sig_2504 = sig_827 & sig_2089;
assign sig_2545 = sig_211;
assign sig_2643 = sig_997 & sig_2009;
assign sig_2740 = sig_827 ^ sig_2089;
assign sig_2805 = sig_1931 & sig_2176;
assign sig_2936 = sig_2421 | sig_2643;
assign sig_2965 = sig_2545;
assign sig_3002 = ~ (sig_2150 | sig_654);
assign sig_3008 = sig_365 & sig_2440;
assign sig_3267 = sig_2805 | sig_3008;
assign sig_3272 = sig_2740 ^ sig_280;
assign sig_3284 = sig_2424 ^ sig_3267;
assign sig_3321 = sig_85;
assign sig_3344 = sig_1311;
assign sig_3352 = sig_2424 & sig_3267;
assign sig_3383 = sig_280 & sig_2740;
assign sig_3438 = sig_3272 & sig_2936;
assign sig_3452 = sig_2504 | sig_3383;
assign sig_3475 = sig_1015 ^ sig_3452;
assign sig_3476 = sig_3284 ^ sig_158;
assign sig_3613 = sig_158 & sig_3284;
assign sig_3628 = sig_1087 & sig_3321;
assign sig_3647 = sig_3002 | sig_3344;
assign sig_3671 = sig_197;
assign sig_3718 = sig_1015 & sig_3452;
assign sig_3734 = sig_1219 & sig_3475;
assign sig_3737 = sig_3475 ^ sig_1219;
assign sig_3748 = sig_2079;
assign sig_3760 = sig_3272 ^ sig_2936;
assign sig_3764 = A[30] | sig_3671;
assign sig_3849 = sig_2485 ^ sig_3647;
assign sig_3862 = sig_735 & sig_3849;
assign sig_3953 = sig_2485 & sig_3647;
assign sig_3974 = sig_3760 ^ sig_481;
assign sig_3992 = sig_481 & sig_3760;
assign sig_4019 = sig_3953 | sig_3862;
assign sig_4087 = sig_3352 | sig_3613;
assign sig_4191 = sig_3718 | sig_3734;
assign sig_4230 = sig_3849 ^ sig_735;
assign sig_4402 = sig_3438 | sig_3992;
assign sig_4446 = sig_810 ^ sig_4191;
assign sig_4461 = sig_4230 & sig_3748;
assign sig_4494 = sig_3748 ^ sig_4230;
assign sig_4511 = sig_3628 | sig_4461;
assign sig_4516 = sig_4191 & sig_810;
assign sig_4532 = sig_4511 & sig_3476;
assign sig_4643 = sig_3974 ^ sig_4532;
assign sig_4671 = sig_4494;
assign sig_4696 = sig_4532 & sig_3974;
assign sig_4738 = sig_3737 ^ sig_4696;
assign sig_4834 = sig_3476 ^ sig_4511;
assign sig_4880 = sig_4402 & sig_4738;
assign sig_4902 = sig_3737 & sig_4696;
assign sig_4905 = sig_4738 ^ sig_4402;
assign sig_4915 = sig_4834;
assign sig_4954 = sig_4915;
assign sig_4966 = sig_4902 | sig_4880;
assign sig_4985 = sig_4954 ^ sig_4019;
assign sig_5057 = sig_4494;
assign sig_5080 = sig_5057;
assign sig_5103 = sig_4019 & sig_4954;
assign sig_5112 = ~ (sig_5080 & sig_3764);
assign sig_5142 = sig_2965;
assign sig_5149 = sig_4643;
assign sig_5153 = ~ sig_2545;
assign sig_5182 = sig_4446 & sig_4966;
assign sig_5193 = sig_4446 ^ sig_4966;
assign sig_5215 = sig_3764 & sig_4671;
assign sig_5249 = sig_4087 & sig_5149;
assign sig_5250 = sig_5142;
assign sig_5268 = sig_5149 ^ sig_4087;
assign sig_5310 = sig_5142 | sig_5103;
assign sig_5314 = sig_1041 | sig_5249;
assign sig_5374 = sig_5268 ^ sig_5310;
assign sig_5377 = sig_5268 & sig_5310;
assign sig_5391 = sig_5250 | sig_5215;
assign sig_5393 = sig_4985 ^ sig_5391;
assign sig_5437 = sig_4985;
assign sig_5458 = sig_4905 ^ sig_5314;
assign sig_5460 = sig_4905 & sig_5314;
assign sig_5514 = sig_5112;
assign sig_5518 = sig_5458 & sig_5377;
assign sig_5548 = sig_5460 | sig_5518;
assign sig_5554 = sig_5112;
assign sig_5555 = sig_5458 & sig_5374;
assign sig_5622 = sig_5437;
assign sig_6110 = sig_5622;
assign sig_6323 = ~ sig_5554;
assign sig_6327 = sig_6110;
assign sig_6329 = sig_5555 & sig_6327;
assign sig_6330 = sig_5514;
assign sig_6331 = sig_5374 & sig_6327;
assign sig_6332 = sig_5548 | sig_6329;
assign sig_6333 = sig_5374 ^ sig_6327;
assign sig_6334 = sig_5377 | sig_6331;
assign sig_6335 = sig_5458 ^ sig_6334;
assign sig_6336 = sig_5393 ^ sig_6330;
assign sig_6337 = sig_5193 & sig_6332;
assign sig_6338 = sig_5193 ^ sig_6332;
assign sig_6339 = sig_5182 | sig_6337;
assign sig_6340 = sig_4516 | sig_6339;

assign Z[0] = sig_3272;
assign Z[1] = B[17];
assign Z[2] = sig_6339;
assign Z[3] = sig_3849;
assign Z[4] = sig_1219;
assign Z[5] = sig_5193;
assign Z[6] = sig_1641;
assign Z[7] = sig_6339;
assign Z[8] = sig_5153;
assign Z[9] = sig_1219;
assign Z[10] = sig_1041;
assign Z[11] = sig_6323;
assign Z[12] = sig_4954;
assign Z[13] = A[31];
assign Z[14] = sig_4880;
assign Z[15] = sig_86;
assign Z[16] = sig_3613;
assign Z[17] = sig_4985;
assign Z[18] = sig_2089;
assign Z[19] = sig_6334;
assign Z[20] = sig_3284;
assign Z[21] = sig_5554;
assign Z[22] = B[30];
assign Z[23] = A[10];
assign Z[24] = A[24];
assign Z[25] = sig_158;
assign Z[26] = B[19];
assign Z[27] = A[28];
assign Z[28] = sig_3974;
assign Z[29] = sig_2150;
assign Z[30] = sig_3321;
assign Z[31] = B[12];
assign Z[32] = sig_810;
assign Z[33] = A[3];
assign Z[34] = sig_5548;
assign Z[35] = sig_2079;
assign Z[36] = sig_2643;
assign Z[37] = sig_6338;
assign Z[38] = A[9];
assign Z[39] = sig_4915;
assign Z[40] = A[17];
assign Z[41] = sig_4643;
assign Z[42] = sig_5514;
assign Z[43] = sig_4966;
assign Z[44] = A[10];
assign Z[45] = sig_2643;
assign Z[46] = sig_2545;
assign Z[47] = sig_2009;
assign Z[48] = sig_5437;
assign Z[49] = sig_3002;
assign Z[50] = sig_4230;
assign Z[51] = sig_810;
assign Z[52] = B[29];
assign Z[53] = sig_158;
assign Z[54] = sig_4954;
assign Z[55] = sig_3008;
assign Z[56] = sig_3764;
assign Z[57] = sig_2545;
assign Z[58] = sig_6323;
assign Z[59] = sig_6336;
assign Z[60] = sig_6333;
assign Z[61] = sig_6335;
assign Z[62] = sig_6338;
assign Z[63] = sig_6340;

endmodule
