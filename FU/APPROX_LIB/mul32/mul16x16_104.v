// Library = EvoApprox 16x16
// Circuit = mul16x16_104
// Area   (45) = 369
// Delay  (45) = 1.240
// Power  (45) = 0.15
// MAE = 316764147389671488.00000
// MRE = 569.82 %
// WCE = 902962033149461248
// WCE% = 21023723137.320 %
// EP = 100.0 %
module mul16x16_104 (
A,
B,
Z
);

input [31:0] A;
input [31:0] B;
output [63:0] Z;

wire sig_69;
wire sig_158;
wire sig_187;
wire sig_242;
wire sig_268;
wire sig_280;
wire sig_365;
wire sig_435;
wire sig_481;
wire sig_504;
wire sig_528;
wire sig_654;
wire sig_683;
wire sig_735;
wire sig_810;
wire sig_827;
wire sig_951;
wire sig_966;
wire sig_997;
wire sig_1015;
wire sig_1058;
wire sig_1097;
wire sig_1208;
wire sig_1219;
wire sig_1245;
wire sig_1265;
wire sig_1311;
wire sig_1315;
wire sig_1361;
wire sig_1393;
wire sig_1395;
wire sig_1405;
wire sig_1567;
wire sig_1641;
wire sig_1668;
wire sig_1779;
wire sig_1931;
wire sig_1973;
wire sig_2009;
wire sig_2077;
wire sig_2079;
wire sig_2089;
wire sig_2176;
wire sig_2275;
wire sig_2421;
wire sig_2424;
wire sig_2440;
wire sig_2485;
wire sig_2504;
wire sig_2643;
wire sig_2647;
wire sig_2740;
wire sig_2792;
wire sig_2805;
wire sig_2936;
wire sig_2955;
wire sig_3002;
wire sig_3008;
wire sig_3267;
wire sig_3272;
wire sig_3284;
wire sig_3325;
wire sig_3344;
wire sig_3352;
wire sig_3372;
wire sig_3383;
wire sig_3438;
wire sig_3452;
wire sig_3475;
wire sig_3476;
wire sig_3580;
wire sig_3613;
wire sig_3626;
wire sig_3631;
wire sig_3647;
wire sig_3718;
wire sig_3734;
wire sig_3737;
wire sig_3740;
wire sig_3748;
wire sig_3760;
wire sig_3764;
wire sig_3849;
wire sig_3862;
wire sig_3953;
wire sig_3974;
wire sig_3992;
wire sig_4019;
wire sig_4087;
wire sig_4116;
wire sig_4191;
wire sig_4230;
wire sig_4402;
wire sig_4412;
wire sig_4446;
wire sig_4461;
wire sig_4494;
wire sig_4511;
wire sig_4516;
wire sig_4532;
wire sig_4554;
wire sig_4639;
wire sig_4643;
wire sig_4671;
wire sig_4696;
wire sig_4709;
wire sig_4738;
wire sig_4825;
wire sig_4834;
wire sig_4864;
wire sig_4880;
wire sig_4888;
wire sig_4902;
wire sig_4905;
wire sig_4915;
wire sig_4954;
wire sig_4966;
wire sig_4985;
wire sig_4998;
wire sig_5023;
wire sig_5046;
wire sig_5057;
wire sig_5080;
wire sig_5103;
wire sig_5112;
wire sig_5142;
wire sig_5149;
wire sig_5182;
wire sig_5193;
wire sig_5215;
wire sig_5228;
wire sig_5249;
wire sig_5250;
wire sig_5268;
wire sig_5310;
wire sig_5314;
wire sig_5374;
wire sig_5377;
wire sig_5391;
wire sig_5393;
wire sig_5437;
wire sig_5458;
wire sig_5460;
wire sig_5518;
wire sig_5548;
wire sig_5554;
wire sig_5555;
wire sig_5622;
wire sig_6110;
wire sig_6323;
wire sig_6327;
wire sig_6329;
wire sig_6331;
wire sig_6332;
wire sig_6333;
wire sig_6334;
wire sig_6335;
wire sig_6336;
wire sig_6337;
wire sig_6338;
wire sig_6339;
wire sig_6340;

assign sig_69 = ~ B[23];
assign sig_158 = A[28] & B[31];
assign sig_187 = ~ A[27];
assign sig_242 = A[26] & A[27];
assign sig_268 = A[31] & B[27];
assign sig_280 = A[30] & B[30];
assign sig_365 = A[28] & B[30];
assign sig_435 = A[29] & B[29];
assign sig_481 = A[29] & B[31];
assign sig_504 = A[31] & B[28];
assign sig_528 = ~ A[18];
assign sig_654 = sig_481 | B[28];
assign sig_683 = A[30] & B[27];
assign sig_735 = A[27] & B[31];
assign sig_810 = A[31] & B[31];
assign sig_827 = A[31] & B[29];
assign sig_951 = ~ A[25];
assign sig_966 = A[30] & B[28];
assign sig_997 = A[29] & B[30];
assign sig_1015 = A[31] & B[30];
assign sig_1058 = A[30] & B[29];
assign sig_1097 = A[31];
assign sig_1208 = A[30] & B[26];
assign sig_1219 = A[30] & B[31];
assign sig_1245 = sig_268 ^ sig_966;
assign sig_1265 = A[31] & B[26];
assign sig_1311 = A[27] & B[30];
assign sig_1315 = A[28] & B[29];
assign sig_1361 = sig_1208 & sig_1097;
assign sig_1393 = ~ A[27];
assign sig_1395 = sig_268 & sig_966;
assign sig_1405 = sig_504 ^ sig_1058;
assign sig_1567 = sig_683 | sig_654;
assign sig_1641 = sig_683;
assign sig_1668 = sig_1265 ^ sig_1361;
assign sig_1779 = sig_435 & sig_1245;
assign sig_1931 = sig_1245 ^ sig_435;
assign sig_1973 = sig_1395 | sig_1779;
assign sig_2009 = sig_1405 ^ sig_1973;
assign sig_2077 = sig_1315 & sig_1567;
assign sig_2079 = sig_1361;
assign sig_2089 = sig_1058 & sig_504;
assign sig_2176 = sig_1641 | sig_2077;
assign sig_2275 = sig_187 | sig_1315;
assign sig_2421 = sig_1405 & sig_1973;
assign sig_2424 = sig_2009 ^ sig_997;
assign sig_2440 = sig_1931 ^ sig_2176;
assign sig_2485 = sig_2440 ^ sig_365;
assign sig_2504 = sig_827 & sig_2089;
assign sig_2643 = sig_997 & sig_2009;
assign sig_2647 = ~ sig_69;
assign sig_2740 = sig_827 ^ sig_2089;
assign sig_2792 = ~ sig_2275;
assign sig_2805 = sig_1931 & sig_2176;
assign sig_2936 = sig_2421 | sig_2643;
assign sig_2955 = ~ sig_2792;
assign sig_3002 = sig_2275 & sig_2792;
assign sig_3008 = sig_365 & sig_2440;
assign sig_3267 = sig_2805 | sig_3008;
assign sig_3272 = sig_2740 ^ sig_280;
assign sig_3284 = sig_2424 ^ sig_3267;
assign sig_3325 = sig_1668;
assign sig_3344 = sig_1311 & sig_2955;
assign sig_3352 = sig_2424 & sig_3267;
assign sig_3372 = sig_2955 | sig_1311;
assign sig_3383 = sig_280 & sig_2740;
assign sig_3438 = sig_3272 & sig_2936;
assign sig_3452 = sig_2504 | sig_3383;
assign sig_3475 = sig_1015 ^ sig_3452;
assign sig_3476 = sig_3284 ^ sig_158;
assign sig_3580 = ~ sig_3383;
assign sig_3613 = sig_158 & sig_3284;
assign sig_3626 = sig_3325;
assign sig_3631 = sig_3372;
assign sig_3647 = sig_3002 | sig_3344;
assign sig_3718 = sig_1015 & sig_3452;
assign sig_3734 = sig_1219 & sig_3475;
assign sig_3737 = sig_3475 ^ sig_1219;
assign sig_3740 = sig_3325;
assign sig_3748 = sig_2079;
assign sig_3760 = sig_3272 ^ sig_2936;
assign sig_3764 = sig_3631;
assign sig_3849 = sig_2485 ^ sig_3647;
assign sig_3862 = sig_735 & sig_3849;
assign sig_3953 = sig_2485 & sig_3647;
assign sig_3974 = sig_3760 ^ sig_481;
assign sig_3992 = sig_481 & sig_3760;
assign sig_4019 = sig_3953 | sig_3862;
assign sig_4087 = sig_3352 | sig_3613;
assign sig_4116 = ~ sig_242;
assign sig_4191 = sig_3718 | sig_3734;
assign sig_4230 = sig_3849 ^ sig_735;
assign sig_4402 = sig_3438 | sig_3992;
assign sig_4412 = sig_3626 & sig_4116;
assign sig_4446 = sig_810 ^ sig_4191;
assign sig_4461 = sig_4230 & sig_2079;
assign sig_4494 = sig_3748 ^ sig_4230;
assign sig_4511 = sig_4461 | sig_4461;
assign sig_4516 = sig_4191 & sig_810;
assign sig_4532 = sig_4511 & sig_3476;
assign sig_4554 = ~ A[5];
assign sig_4639 = sig_3740;
assign sig_4643 = sig_3974 ^ sig_4532;
assign sig_4671 = sig_4494 ^ sig_4639;
assign sig_4696 = sig_4532 & sig_3974;
assign sig_4709 = sig_4639 & sig_4494;
assign sig_4738 = sig_3737 ^ sig_4696;
assign sig_4825 = ~ sig_4412;
assign sig_4834 = sig_3476 ^ sig_4461;
assign sig_4864 = sig_4825 & sig_4671;
assign sig_4880 = sig_4402 & sig_4738;
assign sig_4888 = sig_4709;
assign sig_4902 = sig_3737 & sig_4696;
assign sig_4905 = sig_4738 ^ sig_4402;
assign sig_4915 = sig_4834 ^ sig_4709;
assign sig_4954 = sig_4915 ^ sig_4864;
assign sig_4966 = sig_4902 | sig_4880;
assign sig_4985 = sig_4954 ^ sig_4019;
assign sig_4998 = sig_4709 & sig_3476;
assign sig_5023 = sig_2504 & sig_4888;
assign sig_5046 = sig_4643 & sig_4998;
assign sig_5057 = sig_4671 ^ sig_4825;
assign sig_5080 = sig_5057;
assign sig_5103 = sig_4019 & sig_4954;
assign sig_5112 = sig_5080 ^ sig_3764;
assign sig_5142 = sig_4915 & sig_4864;
assign sig_5149 = sig_4643 ^ sig_4998;
assign sig_5182 = sig_4446 & sig_4966;
assign sig_5193 = sig_4446 ^ sig_4966;
assign sig_5215 = sig_3764 & sig_5080;
assign sig_5228 = ~ B[8];
assign sig_5249 = sig_4087 & sig_5149;
assign sig_5250 = sig_4461 & sig_5023;
assign sig_5268 = sig_5149 ^ sig_4087;
assign sig_5310 = sig_5142 | sig_5103;
assign sig_5314 = sig_5046 | sig_5249;
assign sig_5374 = sig_5268 ^ sig_5310;
assign sig_5377 = sig_5268 & sig_5310;
assign sig_5391 = sig_5250 | sig_5215;
assign sig_5393 = sig_4985 ^ sig_5215;
assign sig_5437 = sig_4985 & sig_5391;
assign sig_5458 = sig_4905 ^ sig_5314;
assign sig_5460 = sig_4905 & sig_5314;
assign sig_5518 = sig_5458 & sig_5377;
assign sig_5548 = sig_5460 | sig_5518;
assign sig_5554 = sig_5112;
assign sig_5555 = sig_5458 & sig_5374;
assign sig_5622 = sig_5437;
assign sig_6110 = sig_5622;
assign sig_6323 = sig_5554;
assign sig_6327 = sig_6110;
assign sig_6329 = sig_5555 & sig_6327;
assign sig_6331 = sig_5374 & sig_6327;
assign sig_6332 = sig_5548 | sig_6329;
assign sig_6333 = sig_5374 ^ sig_6327;
assign sig_6334 = sig_5377 | sig_6331;
assign sig_6335 = sig_5458 ^ sig_6334;
assign sig_6336 = sig_5393;
assign sig_6337 = sig_5193 & sig_6332;
assign sig_6338 = sig_5193 ^ sig_6332;
assign sig_6339 = sig_5182 | sig_6337;
assign sig_6340 = sig_4516 | sig_6339;

assign Z[0] = sig_5310;
assign Z[1] = sig_5437;
assign Z[2] = sig_3740;
assign Z[3] = A[1];
assign Z[4] = sig_435;
assign Z[5] = sig_3953;
assign Z[6] = sig_3580;
assign Z[7] = sig_2936;
assign Z[8] = A[0];
assign Z[9] = B[10];
assign Z[10] = A[23];
assign Z[11] = sig_5057;
assign Z[12] = sig_4888;
assign Z[13] = A[0];
assign Z[14] = sig_1311;
assign Z[15] = sig_683;
assign Z[16] = A[29];
assign Z[17] = sig_4954;
assign Z[18] = sig_6333;
assign Z[19] = sig_4087;
assign Z[20] = A[10];
assign Z[21] = sig_3352;
assign Z[22] = B[15];
assign Z[23] = sig_951;
assign Z[24] = sig_3718;
assign Z[25] = sig_4554;
assign Z[26] = sig_5518;
assign Z[27] = sig_5193;
assign Z[28] = sig_1931;
assign Z[29] = sig_5460;
assign Z[30] = sig_6327;
assign Z[31] = sig_6337;
assign Z[32] = B[9];
assign Z[33] = sig_3272;
assign Z[34] = B[8];
assign Z[35] = sig_4696;
assign Z[36] = sig_2647;
assign Z[37] = sig_2077;
assign Z[38] = sig_187;
assign Z[39] = sig_5080;
assign Z[40] = sig_3383;
assign Z[41] = sig_528;
assign Z[42] = B[29];
assign Z[43] = sig_3631;
assign Z[44] = A[25];
assign Z[45] = sig_1097;
assign Z[46] = sig_1208;
assign Z[47] = sig_2077;
assign Z[48] = sig_1779;
assign Z[49] = A[27];
assign Z[50] = sig_2485;
assign Z[51] = sig_4087;
assign Z[52] = sig_69;
assign Z[53] = sig_5228;
assign Z[54] = sig_1393;
assign Z[55] = sig_2792;
assign Z[56] = sig_1567;
assign Z[57] = sig_3325;
assign Z[58] = sig_6323;
assign Z[59] = sig_6336;
assign Z[60] = sig_6333;
assign Z[61] = sig_6335;
assign Z[62] = sig_6338;
assign Z[63] = sig_6340;

endmodule
