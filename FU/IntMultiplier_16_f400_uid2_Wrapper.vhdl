--------------------------------------------------------------------------------
--                           IntMultiplier_16_f400_uid2
--                      (IntMultiplierClassical_16_f400_uid4)
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Bogdan Pasca, Florent de Dinechin (2008-2010)
--------------------------------------------------------------------------------
-- Pipeline depth: 0 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntMultiplier_16_f400_uid2 is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(15 downto 0);
          Y : in  std_logic_vector(15 downto 0);
          R : out  std_logic_vector(31 downto 0)   );
end entity;

architecture arch of IntMultiplier_16_f400_uid2 is
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
         end if;
      end process;
   --Classical
    R <= X * Y;
end architecture;

--------------------------------------------------------------------------------
--                       IntMultiplier_16_f400_uid2_Wrapper
-- This operator is part of the Infinite Virtual Library FloPoCoLib
-- All rights reserved 
-- Authors: Florent de Dinechin (2007)
--------------------------------------------------------------------------------
-- Pipeline depth: 2 cycles

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library std;
use std.textio.all;
library work;

entity IntMultiplier_16_f400_uid2_Wrapper is
   port ( clk, rst : in std_logic;
          X : in  std_logic_vector(15 downto 0);
          Y : in  std_logic_vector(15 downto 0);
          R : out  std_logic_vector(31 downto 0)   );
end entity;

architecture arch of IntMultiplier_16_f400_uid2_Wrapper is
   component IntMultiplier_16_f400_uid2 is
      port ( clk, rst : in std_logic;
             X : in  std_logic_vector(15 downto 0);
             Y : in  std_logic_vector(15 downto 0);
             R : out  std_logic_vector(31 downto 0)   );
   end component;

signal i_X, i_X_d1 :  std_logic_vector(15 downto 0);
signal i_Y, i_Y_d1 :  std_logic_vector(15 downto 0);
signal o_R, o_R_d1 :  std_logic_vector(31 downto 0);
begin
   process(clk)
      begin
         if clk'event and clk = '1' then
            i_X_d1 <=  i_X;
            i_Y_d1 <=  i_Y;
            o_R_d1 <=  o_R;
         end if;
      end process;
   i_X <= X;
   i_Y <= Y;
   ----------------Synchro barrier, entering cycle 1----------------
   test: IntMultiplier_16_f400_uid2  -- pipelineDepth=0 maxInDelay=8.6272e-10
      port map ( clk  => clk,
                 rst  => rst,
                 R => o_R,
                 X => i_X_d1,
                 Y => i_Y_d1);
   ----------------Synchro barrier, entering cycle 2----------------
   R <= o_R_d1;
end architecture;

