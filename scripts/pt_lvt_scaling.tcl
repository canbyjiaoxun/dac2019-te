#set dname IntAdder_32_f400_uid2_Wrapper_F400_uid8  
#set dname IntMultiplier_UsingDSP_16_16_32_unsigned_F400_uid2_Wrapper_F400_uid6
p
set dname FPAdder_8_23_uid2_Wrapper
#set dname FPMult_8_23_8_23_8_23_F400_uid2_Wrapper_F400_uid33

suppress_message {RC-011}
suppress_message {ENV-003}
suppress_message {PTE-003}
set_host_options -max_cores 2
set power_enable_analysis true
# 0.72V,0C,SS ---> WCZ0D72COM, 
set WCZ072_NVT "/home/TSMC/45GS_GP_CCS_NVT/TSMCHOME/digital/Front_End/timing_power_noise/CCS/tcbn45gsbwp_120a/tcbn45gsbwpwcz0d72_ccs.db"
set WCZ072_LVT "/home/TSMC/45GS_GP_CCS_LVT/TSMCHOME/digital/Front_End/timing_power_noise/CCS/tcbn45gsbwplvt_120a/tcbn45gsbwplvtwcz0d72_ccs.db"
set WCZ072_HVT "/home/TSMC/45GS_GP_CCS_HVT/TSMCHOME/digital/Front_End/timing_power_noise/CCS/tcbn45gsbwphvt_120a/tcbn45gsbwphvtwcz0d72_ccs.db"
# 0.72V,125C,SS ---> WC0D72COM,
set WC072_NVT "/home/TSMC/45GS_GP_CCS_NVT/TSMCHOME/digital/Front_End/timing_power_noise/CCS/tcbn45gsbwp_120a/tcbn45gsbwpwc0d72_ccs.db"
set WC072_LVT "/home/TSMC/45GS_GP_CCS_LVT/TSMCHOME/digital/Front_End/timing_power_noise/CCS/tcbn45gsbwplvt_120a/tcbn45gsbwplvtwc0d72_ccs.db"
set WC072_HVT "/home/TSMC/45GS_GP_CCS_HVT/TSMCHOME/digital/Front_End/timing_power_noise/CCS/tcbn45gsbwphvt_120a/tcbn45gsbwphvtwc0d72_ccs.db"
# 0.81V,0C,SS ---> WCZCOM, 
set WCZ081_NVT "/home/TSMC/45GS_GP_CCS_NVT/TSMCHOME/digital/Front_End/timing_power_noise/CCS/tcbn45gsbwp_120a/tcbn45gsbwpwcz_ccs.db"
set WCZ081_LVT "/home/TSMC/45GS_GP_CCS_LVT/TSMCHOME/digital/Front_End/timing_power_noise/CCS/tcbn45gsbwplvt_120a/tcbn45gsbwplvtwcz_ccs.db"
set WCZ081_HVT "/home/TSMC/45GS_GP_CCS_HVT/TSMCHOME/digital/Front_End/timing_power_noise/CCS/tcbn45gsbwphvt_120a/tcbn45gsbwphvtwcz_ccs.db"
# 0.81V,125C,SS ---> WCCOM, 
set WC081_NVT "/home/TSMC/45GS_GP_CCS_NVT/TSMCHOME/digital/Front_End/timing_power_noise/CCS/tcbn45gsbwp_120a/tcbn45gsbwpwc_ccs.db"
set WC081_LVT "/home/TSMC/45GS_GP_CCS_LVT/TSMCHOME/digital/Front_End/timing_power_noise/CCS/tcbn45gsbwplvt_120a/tcbn45gsbwplvtwc_ccs.db"
set WC081_HVT "/home/TSMC/45GS_GP_CCS_HVT/TSMCHOME/digital/Front_End/timing_power_noise/CCS/tcbn45gsbwphvt_120a/tcbn45gsbwphvtwc_ccs.db"
set link_path "* $WCZ072_NVT $WCZ072_LVT $WCZ072_HVT"

read_verilog ../PL/$dname.pl.v
#just set the current name as what operator is 
current_design $dname
read_parasitics ../PL/$dname.spef.max
current_design
report_design
define_scaling_lib_group -name g1 "$WCZ072_NVT $WC072_NVT $WCZ081_NVT $WC081_NVT"
define_scaling_lib_group -name g2 "$WCZ072_LVT $WC072_LVT $WCZ081_LVT $WC081_LVT"
define_scaling_lib_group -name g3 "$WCZ072_HVT $WC072_HVT $WCZ081_HVT $WC081_HVT"
report_lib_groups -scaling -show {voltage temperature}

set vdds [list 0.72 0.74 0.76 0.78 0.80]
set temps [list 0]
foreach temp $temps {
   foreach vdd $vdds {
	create_operating_conditions -name WC_TV=$temp.$vdd -library tcbn45gsbwpwc_ccs -process 1.00 -temperature $temp -voltage $vdd
	set_operating_conditions WC_TV=$temp.$vdd -library tcbn45gsbwpwc_ccs
	create_clock -period 1.5 clk
	set_propagated_clock [all_clocks]
	update_timing -full
	report_timing -voltage -group clk > ../REP/PT_t_$dname.$vdd.$temp.rep
	report_power > ../REP/PT_p_$dname.$vdd.$temp.rep
	write_sdf ../SDF/$dname.$vdd.$temp.sdf
    }
}

exit

