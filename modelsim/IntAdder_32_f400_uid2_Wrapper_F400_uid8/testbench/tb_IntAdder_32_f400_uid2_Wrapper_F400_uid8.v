`timescale 1ns/1ps

module tb(output reg [31:0] X,
    output reg [31:0] Y,
    output reg Cin,
    output [31:0] R_valid,
    output [31:0] R_PL_valid,
    output reg clk, 
    output reg rst);
    

    reg [63:0] FF_error_cnt;
    integer N, L, M, K, test1, test2;
    parameter L4 = 32;
    integer error_cnt [0:31];
    integer cnt_cycle, err_cycle, err_yes;
    integer TC_file, stat_file, outport_file, in_file, config_file, statusI, c_out;
    wire [31:0] c1, c2;
    real  input_delay, capture_delay, half_period, clk_period;
    

    IntAdder_32_f400_uid2_Wrapper_F400_uid8 UUT2(.clk(clk), .rst(rst), .X(X), .Y(Y), .Cin(Cin),  .R(R_valid));
    IntAdder_32_f400_uid2_Wrapper_F400_uid8_pl UUT3(.clk(clk), .rst(rst), .X(X), .Y(Y), .Cin(Cin), .R(R_PL_valid));
 
    initial
    begin       
       TC_file = $fopen("../REP/TC.txt", "w");  
       config_file = $fopen("../sim/configure_IntAdder_32_f400_uid2_Wrapper_F400_uid8.txt","r");
       stat_file = $fopen("../REP/stat.txt", "w");  
       statusI = $fscanf(config_file,"%f",clk_period);
       $fclose(config_file);
       half_period=0.5*clk_period;
       $display("clock period is: %f", clk_period);
       input_delay=0.2*half_period;
       capture_delay=0.2*half_period; 
     end


    initial
     begin
         clk = 0;
         forever  begin
                  #half_period  clk = !clk; //define the clock period  
         end
     end
         
    initial 
     begin
       M=32;
       in_file  = $fopen("../stimuli_data/data", "r");
       rst=1; 
       cnt_cycle=0;
       err_cycle = 0; 
       test1=0;
       test2=0;
       for(N=0; N<32; N=N+1)
       begin
           error_cnt[N]=0;
       end
       #10 rst=0;
    end  
       
    always @(negedge clk)
    begin
      #input_delay 
      test1=$fscanf(in_file, "%b\t%b\t%b\n", X, Y, Cin);
      if(test1!==3)
      begin
          for(L=0; L<32; L=L+1)
             $fdisplay(stat_file,"cycle %d : bit_error_cnt[%d] = %d",cnt_cycle,L,error_cnt[L]);
          $fdisplay(stat_file, "total error cycle: %d", err_cycle);
          $fclose(in_file);
          $finish; 
      end
      cnt_cycle=cnt_cycle+1;
    end

    always @(negedge clk)
    begin 
      #capture_delay
      err_yes = 0; 
      for(L=L4-1; L>=0; L=L-1) 
      begin
           if (R_PL_valid[L]==R_valid[L])
               $fwrite(TC_file, "0");
           else 
           begin
               $fwrite(TC_file, "1");
               error_cnt[L]<=error_cnt[L]+1;
               err_yes = err_yes + 1; 
           end
      end
      $fwrite(TC_file, "\n"); 
      if (err_yes > 0)
           err_cycle = err_cycle + 1; 
    end      
endmodule
