#Xun 10/24/2016
#Function: used to simulate bit timing error. 
#pre-required: 
#1) work/ library with TSMC cells compiled, no GUI needed 
#2) change the post-layout module name to add PL following top module name 
use File::Copy;

$dname = "IntAdder_32_f400_uid2_Wrapper_F400_uid8";
@vdd_list = ("0.81");
@temp_list = ("50");
@clock_list = ("1.2");
@data_list = ("random_data");


system("vcom -quiet -93 -work work ~/Dropbox/DAC2019-TE/EXACT_LIB/$dname.vhdl");
#system("vlog -quiet -93 -work work /home/jiaoxun/work2/PS/$dname.ps.v");
system("vlog -quiet -93 -work work ../../../PL/$dname.pl.v");

foreach $volt (@vdd_list)
{
  foreach $temp (@temp_list)
  {
    foreach $data (@data_list)
    {
         foreach $clock_period (@clock_list)
         {
	        system("rm -rf configure_$dname.txt");
	        system("echo $clock_period >> configure_$dname.txt");
                system("cp ../stimuli_data/$data ../stimuli_data/data");
                system("vlog -quiet -93 -work work ../testbench/tb_$dname.v");
	        system("vsim -c -sdfnoerror -sdfnowarn +no_tchk_msg -sdfmax tb/UUT3=../../../SDF/$dname.$volt.$temp.sdf work.tb -do \"set StdArithNoWarnings 1; run -all\"");
		#system("vsim -c work.tb -do \"set StdArithNoWarnings 1; run -all\"");
                system("mv ../REP/TC.txt ../REP/TC_$dname.$volt.$temp.$clock_period.$data.txt");          
                system("mv ../REP/stat.txt ../REP/stat_$dname.$volt.$temp.$clock_period.$data.txt");       
	}
     }
  }
}

