import time
import os
from random import * 
from collections import defaultdict
import numpy as np
import sklearn
import scipy
from sklearn import svm
from sklearn import tree
from sklearn.ensemble.forest import RandomForestRegressor
from sklearn.ensemble import RandomForestClassifier
#from sklearn.neural_network import MLPClassifier
from sklearn import linear_model
from sklearn import neighbors
from sklearn.metrics import (accuracy_score, precision_score, recall_score, f1_score)


#create the binary vector as input feature
def bin_feature(data_list,i):
    feat = []
    x1 = map(int, data_list[i-1].split()[0])
    x2 = map(int, data_list[i-1].split()[1])
    x3 = map(int, data_list[i].split()[0])
    x4 = map(int, data_list[i].split()[1])
    feat = feat + x1 + x2 + x3 + x4
    #feat = feat + x3 + x4
    return feat

#3 pipeline cycle, so include t, t-1, t-2 cycle 
def bin_product_feature(data_list,i,bit_width):
    feat = []
    x1 = map(int, data_list[i-2].split()[0])
    x2 = map(int, data_list[i-2].split()[1])
    x3 = map(int, data_list[i-1].split()[0])
    x4 = map(int, data_list[i-1].split()[1])
    x5 = map(int, data_list[i].split()[0])
    x6 = map(int, data_list[i].split()[1])
    product1 = map(int,format(int(bin(int(data_list[i-2].split()[0],2)*(int(data_list[i-2].split()[1],2))),2),'0' + str(2*bit_width) + 'b'))
    product2 = map(int,format(int(bin(int(data_list[i-1].split()[0],2)*(int(data_list[i-1].split()[1],2))),2),'0' + str(2*bit_width) + 'b'))
    product3 = map(int,format(int(bin(int(data_list[i].split()[0],2)*(int(data_list[i].split()[1],2))),2),'0' + str(2*bit_width) + 'b'))
    #product1 = [product1[i] for i in range(2*bit_width)][63-bit_position] #bit feature
    #product2 = [product2[i] for i in range(2*bit_width)][63-bit_position] #bit feature
    product1 = [product1[i] for i in range(2*bit_width)] #bit feature
    product2 = [product2[i] for i in range(2*bit_width)] #bit feature
    product3 = [product3[i] for i in range(2*bit_width)] #bit feature
    #feat = feat + x1 + x2 + x3 + x4 + [product1] + [product2] # bit feature 
    feat = feat + x1 + x2 + x3 + x4 + x5 + x6 + product1 + product2 + product3 # value feature 
    return feat

def bit_label(TC_list, line_number, bit_position):
    bit = int(TC_list[line_number][31-bit_position])
    return bit 

#extract value-level timing error
def value_label(TC_list, line_number):
    if '1' in TC_list[line_number]:
        return 1
    else:
        return 0

def read_input_output_list(operator, condition, data, clk):
    input_data_file = open('../../stimuli_data/' + data,'r')
    output_data_file = open("../../REP/TC_" + operator + condition + clk + data + '.txt','r')
    input_data_list = input_data_file.readlines()
    output_data_list = output_data_file.readlines()
    input_data_file.close()
    output_data_file.close()
    return input_data_list, output_data_list 

def clim_predictor(X, y_test, clf):
    start_time = time.time()
    y_pred = clf.predict(X)
    exe_time = time.time() - start_time
    accuracy = accuracy_score(y_test, y_pred)
    #print y_test, list(y_pred)
    #print y_pred[0]
    precision = precision_score(y_test, y_pred)
    recall = recall_score(y_test, y_pred)
    F1 = f1_score(y_test, y_pred) 
    return accuracy, precision, recall, F1, exe_time

def rand_predictor(y_test):
    y_pred = [randint(0,1) for i in range(len(y_test))] 
    accuracy = accuracy_score(y_test, y_pred)
    precision = precision_score(y_test, y_pred)
    recall = recall_score(y_test, y_pred)
    F1 = f1_score(y_test, y_pred) 
    #error = sum(abs(randint(0,1)-int(test_y[i])) for i in range(len(test_y)))
    #accuracy = 1-float(error)/len(test_y)
    return accuracy, precision, recall, F1   
    #return accuracy

def naive_predictor(y_test):
    y_pred = [0 for i in range(len(y_test))]
    accuracy = accuracy_score(y_test, y_pred)
    precision = precision_score(y_test, y_pred)
    recall = recall_score(y_test, y_pred)
    F1 = f1_score(y_test, y_pred) 
    #error = sum(abs(int(test_y[i])-0) for i in range(len(test_y)))
    #accuracy = 1-float(error)/len(test_y)
    return accuracy, precision, recall, F1

def write_result(bit_position, clk, test_data, ML_accuracy,ML_precision,ML_recall, ML_F1, ML_exe_time, naive_accuracy,naive_precision, naive_recall,  naive_F1, random_accuracy, random_precision, random_recall, random_F1):
    prediction_file = open('MLP_bit_error_prediction_result','a')
    prediction_file.write('################ For CLK at ' + clk + ' at bit ' + str(bit_position) + ' for ' + test_data + '%#################\n')
    prediction_file.write("--- " + str(ML_exe_time) + " seconds ---\n")
    prediction_file.write("Prediction accuracy for clf1 is " +  str(ML_accuracy) + ',' +str(ML_precision)+ ',' + str(ML_recall) + ',' + str(ML_F1) + '\n')
    #prediction_file.write(str(clf1)+'\n')
    prediction_file.write("Prediction accuracy for naive is " + str(naive_accuracy) + ','  +str(naive_precision)+ ',' + str(naive_recall) + ',' + str(naive_F1) + '\n')
    prediction_file.write("Prediction accuracy for random is " + str(random_accuracy) + ',' +str(random_precision)+ ',' + str(random_recall) + ',' + str(random_F1) + '\n')
    prediction_file.close()

def write_value_result(clk, test_data, ML_accuracy,ML_precision,ML_recall, ML_F1, ML_exe_time, naive_accuracy,naive_precision, naive_recall,  naive_F1, random_accuracy, random_precision, random_recall, random_F1):
    prediction_file = open('RFC_value_error_prediction_result','a')
    prediction_file.write('################ For CLK at ' + clk + ' for ' + test_data + '%#################\n')
    prediction_file.write("--- " + str(ML_exe_time) + " seconds ---\n")
    prediction_file.write("Prediction accuracy for clf1 is " +  str(ML_accuracy) + ',' +str(ML_precision)+ ',' + str(ML_recall) + ',' + str(ML_F1) + '\n')
    #prediction_file.write(str(clf1)+'\n')
    prediction_file.write("Prediction accuracy for naive is " + str(naive_accuracy) + ','  +str(naive_precision)+ ',' + str(naive_recall) + ',' + str(naive_F1) + '\n')
    prediction_file.write("Prediction accuracy for random is " + str(random_accuracy) + ',' +str(random_precision)+ ',' + str(random_recall) + ',' + str(random_F1) + '\n')
    prediction_file.close()

def reliability_estimation(X, y_test, clf):
    start_time = time.time()
    y_pred = clf.predict(X)
    exe_time = time.time() - start_time
    clim_reliability = 1-sum(y_pred) / float(len(y_pred))
    gls_reliability = 1-sum(y_test) / float(len(y_test))
    return clim_reliability, gls_reliability, exe_time 

def write_reliability(bit_position, clk, test_data, clim_reliability, gls_reliability, clim_time, condition):
    reliability_file = open('../report/reliability_estimation.' + test_data + condition, 'a')
    reliability_file.write('################ For CLK at ' + clk + ' at bit ' + str(bit_position) + ' for ' + test_data + '%#################\n')
    reliability_file.write("--- " + str(clim_time) + " seconds ---\n")
    #reliability_file.write(str(clf1)+'\n')
    reliability_file.write("Reliability estimation of CLIM and GLS are " +  str(clim_reliability) + ',' +str(gls_reliability) + '\n')
    reliability_file.close()
    return 1

def write_value_reliability(clk, test_data, clim_reliability, gls_reliability, clim_time):
    reliability_file = open('../report/value_reliability_estimation.' + test_data, 'a')
    reliability_file.write('################ For CLK at ' + clk + ' at bit ' + str(bit_position) + ' for ' + test_data + '%#################\n')
    reliability_file.write("--- " + str(clim_time) + " seconds ---\n")
    #reliability_file.write(str(clf1)+'\n')
    reliability_file.write("Reliability estimation of CLIM and GLS are " +  str(clim_reliability) + ',' +str(gls_reliability) + '\n')
    reliability_file.close()
    return 1
