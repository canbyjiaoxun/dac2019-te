#Function: predict bit-level timing errors of FUs using neural networks 
# Xun Jiao 10/06/2018

import useful_function as uf
import time
import os
from random import * 
from collections import defaultdict
import numpy as np
import scipy
import sklearn
from sklearn import svm
from sklearn import tree
from sklearn.ensemble.forest import RandomForestRegressor
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn import linear_model
from sklearn import neighbors
from sklearn.metrics import (accuracy_score, precision_score, recall_score, f1_score)
from sklearn.model_selection import train_test_split


p = True 
operator = os.path.relpath('../..','../../..')
condition_list = ['.0.88.0.','.0.74.0.','.0.78.0.']
clk_list = ["0.69."]
train_data = 'random_data'
test_data = 'random_data'
#we have not compiled any test data yet
#test_data_APP_list  = ["random_data", "sobel_data", "gauss_data"]

for condition in condition_list:
	for clk in clk_list:
		train_data_list, train_bit_list = uf.read_input_output_list(operator, condition, train_data, clk)
		train_size = int(len(train_data_list) * 0.99)
		bit_width = len(train_bit_list[0])- 1

	#Extracting train features
	#train_X1 = [bin_feature(train_data_list,i) for i in range(1,TRAIN)]  #start from the 2nd element of training data because it has preceding input  
		X = [uf.bin_product_feature(train_data_list,i,bit_width) for i in range(2,train_size)]  #start from the 3rd element of training data because it has preceding input  
	######################## FOR EACH BIT POSITION #############################
		for bit_position in range(bit_width):
		####################### FORM TRAINING LABELS #####################################
			y = [uf.bit_label(train_bit_list,i,bit_position) for i in range(5,train_size+3)] # 1st element map to 2nd element 
			train_X, test_X, train_y, test_y = train_test_split(X, y, test_size = 0.10, random_state = 40)

		#clf1 = linear_model.LogisticRegression()
			clf1 = MLPClassifier(alpha=0.5)
		#clf1 = RandomForestClassifier(n_estimators=10)  
		#clf1.fit(train_X1, train_y)
			clf1.fit(train_X, train_y)
			print "model fitting complete"
 

                ################### WRITE RESULT TO FILE ##########################
	        	clim_reliability, gls_reliability, clim_time = uf.reliability_estimation(test_X, test_y, clf1)	
                	print "reliability estimation are  ", clim_reliability, gls_reliability, clim_time
                	uf.write_reliability(bit_position, clk, test_data, clim_reliability, gls_reliability, clim_time, condition)












######################  TRASH ###########################
'''
		#################### WRITE RESULT TO FILE ##########################
		#start_time = time.time()
		ML_accuracy, ML_precision, ML_recall, ML_F1, ML_exe_time = uf.clim_predictor(test_X, test_y, clf1)
		#exe_time = time.time() - start_time 
		naive_accuracy, naive_precision, naive_recall, naive_F1 = uf.naive_predictor(test_y)
		random_accuracy, random_precision, random_recall, random_F1 = uf.rand_predictor(test_y)
		print "accuracy for clf1 is ", ML_accuracy,ML_precision,ML_recall, ML_F1
		print "accuracy for naive is ", naive_accuracy,naive_precision,naive_recall,naive_F1
		print "accuracy for random is ", random_accuracy,random_precision,random_recall, random_F1
		uf.write_result(bit_position, clk, test_data, ML_accuracy,ML_precision,ML_recall, ML_F1, ML_exe_time, naive_accuracy,naive_precision, naive_recall,  naive_F1, random_accuracy, random_precision, random_recall, random_F1)

###################### FOR EACH TEST DATA #########################################
	for test_data in test_data_APP_list: 
		test_data_list,test_bit_list = uf.read_input_output_list(operator, condition, test_data, clk) 
		TEST = int(len(test_data_list) * 0.5)
                print TEST

		#Extract test features
		#test_X1 = [bin_feature(test_data_list,i) for i in range(1,TEST)]
		test_X2 = [uf.bin_sum_feature(test_data_list,i,bit_width) for i in range(1,TEST)]
			#form testing input file 
			test_y = [uf.bit_label(test_bit_list,i,bit_position) for i in range(3,TEST+2)]
'''
